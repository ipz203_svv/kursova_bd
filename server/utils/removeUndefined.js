module.exports = function (filter) {
    let filterDefined = {}
    for (let key in filter) {
        if (filter[key] !== undefined) {
            filterDefined[key] = filter[key]
        }
    }
    return filterDefined
}
