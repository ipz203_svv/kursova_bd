const {Like} = require("../models/models");
const ApiError = require("../error/apiError");

class LikeController {
    async create(req, res, next) {
        try {
            const {
                userId,
                assemblyId
            } = req.body

            const like = Like.create({
                userId,
                assemblyId
            })
            return res.json(like)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async countByAssemblyId(req, res) {
        let {id} = req.params
        const count = await Like.count({where: {assemblyId: id}})
        return res.json(count)
    }

    async findOneLike(req, res) {
        let {
            userId, assemblyId
        } = req.query

        const like = await Like.findOne({where: {userId, assemblyId}})
        return res.json(like)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const like = await Like.destroy({where: {id}})
            return res.json(like)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new LikeController()