const {Case, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class CaseController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                width,
                height,
                innerLength,
                depth,
                maxPsuLength,
                fanCount,
                formFactorId,
                manufacturerId,
                fanSizeId
            } = req.body

            const case_ = await Case.create({
                name,
                releaseYear,
                width,
                height,
                innerLength,
                depth,
                maxPsuLength,
                fanCount,
                formFactorId,
                manufacturerId,
                fanSizeId
            })
            return res.json(case_)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name, formFactorId, manufacturerId, fanSizeId, limit, page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let cases
        let filter = {
            formFactorId, manufacturerId, fanSizeId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                cases = await Case.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                cases = await Case.findAndCountAll({where: {...filterDefined}, limit, offset})
            }

        } else {
            cases = await Case.findAndCountAll({limit, offset})
        }

        return res.json(cases)
    }

    async getOne(req, res) {
        const {id} = req.params
        const case_ = await Case.findOne({where: {id}})
        return res.json(case_)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const case_ = await Case.destroy({where: {id}})
            return res.json(case_)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new CaseController()