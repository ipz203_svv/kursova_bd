const {PowerSupply, Cpu, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class PowerSupplyController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                maxPower,
                rating80Plus,
                psuLength,
                manufacturerId
            } = req.body

            const powerSupply = await PowerSupply.create({
                name,
                releaseYear,
                maxPower,
                rating80Plus,
                psuLength,
                manufacturerId
            })
            return res.json(powerSupply)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name,
            manufacturerId,
            limit,
            page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let powerSupplies
        let filter = {
            manufacturerId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                powerSupplies = await PowerSupply.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                powerSupplies = await PowerSupply.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            powerSupplies = await PowerSupply.findAndCountAll({limit, offset})
        }

        return res.json(powerSupplies)
    }

    async getOne(req, res) {
        const {id} = req.params
        const powerSupply = await PowerSupply.findOne({where: {id}})
        return res.json(powerSupply)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const powerSupply = await PowerSupply.destroy({where: {id}})
            return res.json(powerSupply)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new PowerSupplyController()