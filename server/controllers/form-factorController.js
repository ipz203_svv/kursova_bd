const {FormFactor, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class FormFactorController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body

            const formFactor = await FormFactor.create({
                name
            })
            return res.json(formFactor)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const formFactors = await FormFactor.findAll()
        return res.json(formFactors)
    }

    async getOne(req, res) {
        const {id} = req.params
        const formFactor = await FormFactor.findOne({where: {id}})
        return res.json(formFactor)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const formFactor = await FormFactor.destroy({where: {id}})
            return res.json(formFactor)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new FormFactorController()