const {SsdInterface, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class SsdInterfaceController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body

            const ssdInterface = await SsdInterface.create({
                name
            })
            return res.json(ssdInterface)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const ssdInterfaces = await SsdInterface.findAll()
        return res.json(ssdInterfaces)
    }

    async getOne(req, res) {
        const {id} = req.params
        const ssdInterface = await SsdInterface.findOne({where: {id}})
        return res.json(ssdInterface)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const ssdInterface = await SsdInterface.destroy({where: {id}})
            return res.json(ssdInterface)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new SsdInterfaceController()