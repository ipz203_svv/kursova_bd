const {FanSize, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class FanSizeController {
    async create(req, res, next) {
        try {
            const {
                size
            } = req.body

            const fanSize = await FanSize.create({
                size
            })
            return res.json(fanSize)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const fanSizes = await FanSize.findAll()
        return res.json(fanSizes)
    }

    async getOne(req, res) {
        const {id} = req.params
        const fanSize = await FanSize.findOne({where: {id}})
        return res.json(fanSize)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const fanSize = await FanSize.destroy({where: {id}})
            return res.json(fanSize)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new FanSizeController()