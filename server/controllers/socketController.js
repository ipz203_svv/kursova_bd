const {Socket, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class SocketController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body

            const socket = await Socket.create({
                name
            })
            return res.json(socket)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const sockets = await Socket.findAll()
        return res.json(sockets)
    }

    async getOne(req, res) {
        const {id} = req.params
        const socket = await Socket.findOne({where: {id}})
        return res.json(socket)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const socket = await Socket.destroy({where: {id}})
            return res.json(socket)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new SocketController()