const {CpuCooler, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class CpuCoolerController {
    async create(req, res, next) {
        try {
            const {
                name, releaseYear, height, tdp, lowProfileRam, manufacturerId, socketId
            } = req.body

            const cpuCooler = await CpuCooler.create({
                name, releaseYear, height, tdp, lowProfileRam, manufacturerId, socketId
            })
            return res.json(cpuCooler)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name, manufacturerId, socketId, limit, page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let cpuCoolers
        let filter = {
            manufacturerId, socketId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                cpuCoolers = await CpuCooler.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                cpuCoolers = await CpuCooler.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            cpuCoolers = await CpuCooler.findAndCountAll({limit, offset})
        }

        return res.json(cpuCoolers)
    }

    async getOne(req, res) {
        const {id} = req.params
        const cpuCooler = await CpuCooler.findOne({where: {id}})
        return res.json(cpuCooler)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const cpuCooler = await CpuCooler.destroy({where: {id}})
            return res.json(cpuCooler)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new CpuCoolerController()