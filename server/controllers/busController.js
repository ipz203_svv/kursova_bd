const {Bus, Like} = require('../models/models')
const ApiError = require("../error/apiError");

class BusController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body
            const bus = await Bus.create({
                name
            })
            return res.json(bus)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const busses = await Bus.findAll()
        return res.json(busses)
    }

    async getOne(req, res) {
        const {id} = req.params
        const bus = await Bus.findOne({where: {id}})
        return res.json(bus)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const bus = await Bus.destroy({where: {id}})
            return res.json(bus)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new BusController()