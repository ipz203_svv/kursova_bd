const {Motherboard, Cpu, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class MotherboardController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                chipset,
                ramCount,
                ramMaxCapacity,
                m2Count,
                formFactorId,
                manufacturerId,
                socketId,
                ramTypeId,
                ssdInterfaceId,
                busId
            } = req.body

            const motherboard = await Motherboard.create({
                name,
                releaseYear,
                chipset,
                ramCount,
                ramMaxCapacity,
                m2Count,
                formFactorId,
                manufacturerId,
                socketId,
                ramTypeId,
                ssdInterfaceId,
                busId
            })
            return res.json(motherboard)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name, formFactorId, manufacturerId, socketId, ramTypeId, ssdInterfaceId, busId, limit, page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let motherboards
        let filter = {
            formFactorId, manufacturerId, socketId, ramTypeId, ssdInterfaceId, busId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                motherboards = await Motherboard.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                motherboards = await Motherboard.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            motherboards = await Motherboard.findAndCountAll({limit, offset})
        }

        return res.json(motherboards)
    }

    async getOne(req, res) {
        const {id} = req.params
        const motherboard = await Motherboard.findOne({where: {id}})
        return res.json(motherboard)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const motherboard = await Motherboard.destroy({where: {id}})
            return res.json(motherboard)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new MotherboardController()