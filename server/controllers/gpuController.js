const {Gpu, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class GpuController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                chip,
                memorySize,
                gpuClock,
                memoryClock,
                tdp,
                length,
                gmemoryType,
                manufacturerId,
                busId
            } = req.body

            const gpu = await Gpu.create({
                name,
                releaseYear,
                chip,
                memorySize,
                gpuClock,
                memoryClock,
                tdp,
                length,
                gmemoryType,
                manufacturerId,
                busId
            })
            return res.json(gpu)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name,
            manufacturerId,
            busId,
            limit,
            page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let gpus
        let filter = {
            manufacturerId,
            busId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                gpus = await Gpu.findAndCountAll({where: {...filterDefined, name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')}, limit, offset})
            } else {
                gpus = await Gpu.findAndCountAll({where: {...filterDefined}, limit, offset})
            }

        } else {
            gpus = await Gpu.findAndCountAll({limit, offset})
        }

        return res.json(gpus)
    }

    async getOne(req, res) {
        const {id} = req.params
        const gpu = await Gpu.findOne({where: {id}})
        return res.json(gpu)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const gpu = await Gpu.destroy({where: {id}})
            return res.json(gpu)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new GpuController()