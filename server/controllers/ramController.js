const {Ram, Cpu, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class RamController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                size,
                clock,
                lowProfile,
                manufacturerId,
                ramTypeId
            } = req.body

            const ram = await Ram.create({
                name,
                releaseYear,
                size,
                clock,
                lowProfile,
                manufacturerId,
                ramTypeId
            })
            return res.json(ram)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name,
            manufacturerId,
            ramTypeId,
            limit,
            page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let rams
        let filter = {
            manufacturerId,
            ramTypeId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                rams = await Ram.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                rams = await Ram.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            rams = await Ram.findAndCountAll({limit, offset})
        }

        return res.json(rams)
    }

    async getOne(req, res) {
        const {id} = req.params
        const ram = await Ram.findOne({where: {id}})
        return res.json(ram)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const ram = await Ram.destroy({where: {id}})
            return res.json(ram)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new RamController()