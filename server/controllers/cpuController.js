const {Cpu, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require('../db')

class CpuController {
    async create(req, res, next) {
        try {
            const {
                name, releaseYear, core, thread, clock, process, cache, ramMaxClock, tdp, manufacturerId, socketId
            } = req.body

            const cpu = await Cpu.create({
                name, releaseYear, core, thread, clock, process, cache, ramMaxClock, tdp, manufacturerId, socketId
            })
            return res.json(cpu)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name, manufacturerId, socketId, limit, page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let cpus
        let filter = {
            manufacturerId, socketId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                cpus = await Cpu.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                cpus = await Cpu.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            cpus = await Cpu.findAndCountAll({limit, offset})
        }

        return res.json(cpus)
    }

    async getOne(req, res) {
        const {id} = req.params
        const cpu = await Cpu.findOne({where: {id}})
        return res.json(cpu)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const cpu = await Cpu.destroy({where: {id}})
            return res.json(cpu)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new CpuController()