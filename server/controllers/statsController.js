const sequelize = require('../db')

class StatsController {
    async getUsers(req, res) {
        let {month} = req.query
        const result = await sequelize.query('SELECT COUNT(users.id) FROM users WHERE EXTRACT(MONTH FROM "created_at") = :month', {replacements: {month: month}})
        return res.json(result)
    }

    async getAssemblies(req, res) {
        let {month} = req.query
        const result = await sequelize.query('SELECT COUNT(assemblies.id) FROM assemblies WHERE EXTRACT(MONTH FROM "created_at") = :month', {replacements: {month: month}})
        return res.json(result)
    }
}

module.exports = new StatsController()