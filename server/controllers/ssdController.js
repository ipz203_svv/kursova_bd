const {Ssd, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class SsdController {
    async create(req, res, next) {
        try {
            const {
                name,
                releaseYear,
                size,
                readSpeed,
                writeSpeed,
                manufacturerId,
                ssdInterfaceId
            } = req.body

            const ssd = await Ssd.create({
                name,
                releaseYear,
                size,
                readSpeed,
                writeSpeed,
                manufacturerId,
                ssdInterfaceId
            })
            return res.json(ssd)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name,
            manufacturerId,
            ssdInterfaceId,
            limit,
            page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let ssds
        let filter = {
            manufacturerId,
            ssdInterfaceId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                ssds = await Ssd.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                ssds = await Ssd.findAndCountAll({where: {...filterDefined}, limit, offset})
            }
        } else {
            ssds = await Ssd.findAndCountAll({limit, offset})
        }

        return res.json(ssds)
    }

    async getOne(req, res) {
        const {id} = req.params
        const ssd = await Ssd.findOne({where: {id}})
        return res.json(ssd)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const ssd = await Ssd.destroy({where: {id}})
            return res.json(ssd)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new SsdController()