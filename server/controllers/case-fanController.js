const {CaseFan, Like} = require("../models/models");
const ApiError = require("../error/apiError");
const removeUndefined = require('../utils/removeUndefined')
const sequelize = require("../db");

class CaseFanController {
    async create(req, res, next) {
        try {
            const {
                name, releaseYear, rpm, manufacturerId, fanSizeId
            } = req.body

            const caseFan = await CaseFan.create({
                name, releaseYear, rpm, manufacturerId, fanSizeId
            })
            return res.json(caseFan)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            name, manufacturerId, fanSizeId, limit, page
        } = req.query

        page = page || 1
        limit = limit || 10
        let offset = page * limit - limit
        let caseFans
        let filter = {
            manufacturerId, fanSizeId
        }

        let filterDefined = removeUndefined(filter)

        if (filterDefined) {
            if (name) {
                caseFans = await CaseFan.findAndCountAll({
                    where: {
                        ...filterDefined,
                        name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', '%' + name.toLowerCase() + '%')
                    }, limit, offset
                })
            } else {
                caseFans = await CaseFan.findAndCountAll({where: {...filterDefined}, limit, offset})
            }

        } else {
            caseFans = await CaseFan.findAndCountAll({limit, offset})
        }

        return res.json(caseFans)
    }

    async getOne(req, res) {
        const {id} = req.params
        const caseFan = await CaseFan.findOne({where: {id}})
        return res.json(caseFan)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const caseFan = await CaseFan.destroy({where: {id}})
            return res.json(caseFan)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new CaseFanController()