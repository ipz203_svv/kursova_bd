const {RamType, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class RamTypeController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body

            const ramType = await RamType.create({
                name
            })
            return res.json(ramType)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const ramTypes = await RamType.findAll()
        return res.json(ramTypes)
    }

    async getOne(req, res) {
        const {id} = req.params
        const ramType = await RamType.findOne({where: {id}})
        return res.json(ramType)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const ramType = await RamType.destroy({where: {id}})
            return res.json(ramType)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new RamTypeController()