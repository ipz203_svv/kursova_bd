const {Assembly, sequelize, Like} = require('../models/models')
const ApiError = require('../error/apiError')
const removeUndefined = require('../utils/removeUndefined')

class AssemblyController {
    async create(req, res, next) {
        try {
            const {
                ramCount,
                caseFanCount,
                userId,
                caseId,
                motherboardId,
                cpuId,
                cpuCoolerId,
                ramId,
                gpuId,
                ssdId,
                powerSupplyId,
                caseFanId
            } = req.body

            const assembly = await Assembly.create({
                ramCount,
                caseFanCount,
                userId,
                caseId,
                motherboardId,
                cpuId,
                cpuCoolerId,
                ramId,
                gpuId,
                ssdId,
                powerSupplyId,
                caseFanId
            })
            return res.json(assembly)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        let {
            sort, limit, page, userId
        } = req.query

        page = page || 1
        limit = limit || 5
        let offset = page * limit - limit

        let assemblies

        if (userId) {
            if (sort === 'my') {
                assemblies = await Assembly.findAndCountAll({where: {userId}, order: [['created_at', 'DESC']], offset, limit})
            }
            if (sort === 'liked') {
                const count = await sequelize.query(`SELECT COUNT(assemblies.*) FROM assemblies LEFT JOIN likes ON "assemblyId" = assemblies.id WHERE likes."userId" = ${userId} GROUP BY assemblies.id`)
                const assembliesQuery = await sequelize.query(`SELECT assemblies.*, assemblies.ram_count AS "ramCount", assemblies.case_fan_count AS "caseFanCount" FROM assemblies LEFT JOIN likes ON "assemblyId" = assemblies.id WHERE likes."userId" = ${userId} GROUP BY assemblies.id ORDER BY "created_at" DESC LIMIT ${limit} OFFSET ${offset}`)
                assemblies = {count: parseInt(count[0][0].count), rows: assembliesQuery[0]}
            }
        } else {
            if (sort) {
                if (sort === 'created_at') {
                    assemblies = await Assembly.findAndCountAll({order: [[sort, 'DESC']], offset, limit})
                }
                if (sort === 'likesCount') {
                    const count = await sequelize.query(`SELECT COUNT(*) FROM assemblies`)
                    const assembliesQuery = await sequelize.query(`SELECT assemblies.*, assemblies.ram_count AS "ramCount", assemblies.case_fan_count AS "caseFanCount", COUNT(likes."assemblyId") AS likesCount FROM assemblies LEFT JOIN likes ON "assemblyId" = assemblies.id GROUP BY assemblies.id ORDER BY likesCount DESC LIMIT ${limit} OFFSET ${offset}`)
                    assemblies = {count: parseInt(count[0][0].count), rows: assembliesQuery[0]}
                }
            } else {
                assemblies = await Assembly.findAndCountAll({limit, offset})
            }
        }

        return res.json(assemblies)
    }

    async getOne(req, res) {
        const {id} = req.params
        const assembly = await Assembly.findOne({where: {id}})
        return res.json(assembly)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const assembly = await Assembly.destroy({where: {id}})
            return res.json(assembly)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new AssemblyController()