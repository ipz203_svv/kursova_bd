const {Manufacturer, Like} = require("../models/models");
const ApiError = require("../error/apiError");

class ManufacturerController {
    async create(req, res, next) {
        try {
            const {
                name
            } = req.body

            const manufacturer = await Manufacturer.create({
                name
            })
            return res.json(manufacturer)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const manufacturers = await Manufacturer.findAll()
        return res.json(manufacturers)
    }

    async getOne(req, res) {
        const {id} = req.params
        const manufacturer = await Manufacturer.findOne({where: {id}})
        return res.json(manufacturer)
    }

    async deleteOne(req, res) {
        try {
            const {
                id
            } = req.params

            const manufacturer = await Manufacturer.destroy({where: {id}})
            return res.json(manufacturer)
        } catch (e) {
            return res.json(ApiError.badRequest(e.message))
        }
    }
}

module.exports = new ManufacturerController()