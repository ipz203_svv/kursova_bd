const Router = require('express')
const router = new Router()
const caseFanController = require('../controllers/case-fanController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), caseFanController.create)
router.get('/', caseFanController.getAll)
router.get('/:id', caseFanController.getOne)
router.delete('/:id', checkRole('ADMIN'), caseFanController.deleteOne)

module.exports = router