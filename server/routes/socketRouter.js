const Router = require('express')
const router = new Router()
const socketController = require('../controllers/socketController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), socketController.create)
router.get('/', socketController.getAll)
router.get('/:id', socketController.getOne)
router.delete('/:id', checkRole('ADMIN'), socketController.deleteOne)

module.exports = router