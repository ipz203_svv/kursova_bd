const Router = require('express')
const router = new Router()
const assemblyController = require('../controllers/assemblyController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', assemblyController.create)
router.get('/', assemblyController.getAll)
router.get('/:id', assemblyController.getOne)
router.delete('/:id', checkRole('ADMIN'), assemblyController.deleteOne)

module.exports = router
