const Router = require('express')
const router = new Router()
const cpuController = require('../controllers/cpuController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), cpuController.create)
router.get('/', cpuController.getAll)
router.get('/:id', cpuController.getOne)
router.delete('/:id', checkRole('ADMIN'), cpuController.deleteOne)

module.exports = router