const Router = require('express')
const router = new Router()
const ssdInterfaceController = require('../controllers/ssd-interfaceController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), ssdInterfaceController.create)
router.get('/', ssdInterfaceController.getAll)
router.get('/:id', ssdInterfaceController.getOne)
router.delete('/:id', checkRole('ADMIN'), ssdInterfaceController.deleteOne)

module.exports = router