const Router = require('express')
const router = new Router()
const motherboardController = require('../controllers/motherboardController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), motherboardController.create)
router.get('/', motherboardController.getAll)
router.get('/:id', motherboardController.getOne)
router.delete('/:id', checkRole('ADMIN'), motherboardController.deleteOne)

module.exports = router