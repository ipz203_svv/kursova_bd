const Router = require('express')
const router= new Router()
const StatsController = require('../controllers/statsController.js')
const role = require('../middleware/roleCheckMiddleware.js')

router.get('/users',role('ADMIN'), StatsController.getUsers)
router.get('/assemblies',role('ADMIN'), StatsController.getAssemblies)

module.exports = router