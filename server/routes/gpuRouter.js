const Router = require('express')
const router = new Router()
const gpuController = require('../controllers/gpuController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), gpuController.create)
router.get('/', gpuController.getAll)
router.get('/:id', gpuController.getOne)
router.delete('/:id', checkRole('ADMIN'), gpuController.deleteOne)

module.exports = router