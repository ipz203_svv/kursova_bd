const Router = require('express')
const router = new Router()
const ssdController = require('../controllers/ssdController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), ssdController.create)
router.get('/', ssdController.getAll)
router.get('/:id', ssdController.getOne)
router.delete('/', checkRole('ADMIN'), ssdController.deleteOne)

module.exports = router