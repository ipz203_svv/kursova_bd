const Router = require('express')
const router = new Router()
const fanSizeController = require('../controllers/fan-sizeController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), fanSizeController.create)
router.get('/', fanSizeController.getAll)
router.get('/:id', fanSizeController.getOne)
router.delete('/:id', checkRole('ADMIN'), fanSizeController.deleteOne)

module.exports = router