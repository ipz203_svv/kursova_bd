const Router = require('express')
const router = new Router()
const cpuCoolerController = require('../controllers/cpu-coolerController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), cpuCoolerController.create)
router.get('/', cpuCoolerController.getAll)
router.get('/:id', cpuCoolerController.getOne)
router.delete('/', checkRole('ADMIN'), cpuCoolerController.deleteOne)

module.exports = router