const Router = require('express')
const router = new Router()
const ramTypeController = require('../controllers/ram-typeController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), ramTypeController.create)
router.get('/', ramTypeController.getAll)
router.get('/:id', ramTypeController.getOne)
router.delete('/:id', checkRole('ADMIN'), ramTypeController.deleteOne)

module.exports = router