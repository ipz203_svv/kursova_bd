const Router = require('express')
const router = new Router()
const likeController = require('../controllers/likeController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', likeController.create)
router.get('/', likeController.findOneLike)
router.get('/:id', likeController.countByAssemblyId)
router.delete('/:id', likeController.deleteOne)

module.exports = router