const Router = require('express')
const router = new Router()
const formFactorController = require('../controllers/form-factorController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), formFactorController.create)
router.get('/', formFactorController.getAll)
router.get('/:id', formFactorController.getOne)
router.delete('/:id', checkRole('ADMIN'), formFactorController.deleteOne)

module.exports = router