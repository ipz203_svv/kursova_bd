const Router = require('express')
const router = new Router()
const caseController = require('../controllers/caseController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), caseController.create)
router.get('/', caseController.getAll)
router.get('/:id', caseController.getOne)
router.delete('/:id', checkRole('ADMIN'), caseController.deleteOne)

module.exports = router