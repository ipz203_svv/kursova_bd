const Router = require('express')
const router = new Router()
const powerSupplyController = require('../controllers/power-supplyController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), powerSupplyController.create)
router.get('/', powerSupplyController.getAll)
router.get('/:id', powerSupplyController.getOne)
router.delete('/:id', checkRole('ADMIN'), powerSupplyController.deleteOne)

module.exports = router