const Router = require('express')
const router = new Router()
const manufacturerController = require('../controllers/manufacturerController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), manufacturerController.create)
router.get('/', manufacturerController.getAll)
router.get('/:id', manufacturerController.getOne)
router.delete('/:id', checkRole('ADMIN'), manufacturerController.deleteOne)

module.exports = router