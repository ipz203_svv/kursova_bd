const Router = require('express')
const router = new Router()
const ramController = require('../controllers/ramController')
const checkRole = require('../middleware/roleCheckMiddleware')

router.post('/', checkRole('ADMIN'), ramController.create)
router.get('/', ramController.getAll)
router.get('/:id', ramController.getOne)
router.delete('/:id', checkRole('ADMIN'), ramController.deleteOne)

module.exports = router