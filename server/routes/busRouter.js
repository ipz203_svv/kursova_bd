const Router = require('express')
const router = new Router()
const busController = require('../controllers/busController')
const checkRole = require('../middleware/roleCheckMiddleware')


router.post('/', checkRole('ADMIN'), busController.create)
router.get('/', busController.getAll)
router.get('/:id', busController.getOne)
router.delete('/:id', checkRole('ADMIN'), busController.deleteOne)

module.exports = router
