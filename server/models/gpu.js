module.exports = (sequelize, DataTypes) => {
    const Gpu = sequelize.define('gpu', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        chip: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'chip'
        },
        memorySize: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'memory_size'
        },
        gpuClock: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'gpu_clock'
        },
        memoryClock: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'memory_clock'
        },
        tdp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'tdp'
        },
        length: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'length'
        },
        gmemoryType: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'gmemory_type'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'gpus'});

    return Gpu;
};
