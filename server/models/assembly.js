module.exports = (sequelize, DataTypes) => {
    const Assembly = sequelize.define('assembly', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        ramCount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'ram_count'
        },
        caseFanCount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'case_fan_count'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'assemblies'});

    return Assembly;
};
