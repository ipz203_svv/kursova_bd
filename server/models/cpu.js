module.exports = (sequelize, DataTypes) => {
    const Cpu = sequelize.define('cpu', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        core: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'core'
        },
        thread: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'thread'
        },
        clock: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'clock'
        },
        process: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'process'
        },
        cache: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'cache'
        },
        ramMaxClock: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'ram_max_clock'
        },
        tdp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'tdp'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'cpus'});

    return Cpu;
};
