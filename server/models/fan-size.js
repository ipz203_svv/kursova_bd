module.exports = (sequelize, DataTypes) => {
    const FanSize = sequelize.define('fanSize', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        size: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'size'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'fan_sizes'});

    return FanSize;
};
