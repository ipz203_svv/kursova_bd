const sequelize = require('../db')
const {Sequelize} = require('sequelize')

const UserModel = require('./user.js')
const CaseModel = require('./case.js')
const FormFactorModel = require('./form-factor.js')
const ManufacturerModel = require('./manufacturer.js')
const FanSizeModel = require('./fan-size.js')
const MotherboardModel = require('./motherboard.js')
const SocketModel = require('./socket.js')
const RamTypeModel = require('./ram-type.js')
const SsdInterfaceModel = require('./ssd-interface.js')
const BusModel = require('./bus.js')
const CpuModel = require('./cpu.js')
const CpuCoolerModel = require('./cpu-cooler.js')
const RamModel = require('./ram.js')
const GpuModel = require('./gpu.js')
const SsdModel = require('./ssd.js')
const PowerSupplyModel = require('./power-supply.js')
const CaseFanModel = require('./case-fan.js')
const AssemblyModel = require('./assembly.js')
const LikeModel = require('./like.js')

const User = UserModel(sequelize, Sequelize)
const Case = CaseModel(sequelize, Sequelize)
const FormFactor = FormFactorModel(sequelize, Sequelize)
const Manufacturer = ManufacturerModel(sequelize, Sequelize)
const FanSize = FanSizeModel(sequelize, Sequelize)
const Motherboard = MotherboardModel(sequelize, Sequelize)
const Socket = SocketModel(sequelize, Sequelize)
const RamType = RamTypeModel(sequelize, Sequelize)
const SsdInterface = SsdInterfaceModel(sequelize, Sequelize)
const Bus = BusModel(sequelize, Sequelize)
const Cpu = CpuModel(sequelize, Sequelize)
const CpuCooler = CpuCoolerModel(sequelize, Sequelize)
const Ram = RamModel(sequelize, Sequelize)
const Gpu = GpuModel(sequelize, Sequelize)
const Ssd = SsdModel(sequelize, Sequelize)
const PowerSupply = PowerSupplyModel(sequelize, Sequelize)
const CaseFan = CaseFanModel(sequelize, Sequelize)
const Assembly = AssemblyModel(sequelize, Sequelize)
const Like = LikeModel(sequelize, Sequelize)

User.hasMany(Assembly)
User.hasMany(Like)

Case.belongsTo(FormFactor)
Case.belongsTo(Manufacturer)
Case.belongsTo(FanSize)
Case.hasMany(Assembly)

FormFactor.hasMany(Case)
FormFactor.hasMany(Motherboard)

Manufacturer.hasMany(Case)
Manufacturer.hasMany(Motherboard)
Manufacturer.hasMany(Cpu)
Manufacturer.hasMany(CpuCooler)
Manufacturer.hasMany(Ram)
Manufacturer.hasMany(Gpu)
Manufacturer.hasMany(Ssd)
Manufacturer.hasMany(PowerSupply)
Manufacturer.hasMany(CaseFan)

FanSize.hasMany(Case)
FanSize.hasMany(CaseFan)

Motherboard.belongsTo(Manufacturer)
Motherboard.belongsTo(FormFactor)
Motherboard.belongsTo(Socket)
Motherboard.belongsTo(RamType)
Motherboard.belongsTo(SsdInterface)
Motherboard.belongsTo(Bus)
Motherboard.hasMany(Assembly)

Socket.hasMany(Motherboard)
Socket.hasMany(Cpu)
Socket.hasMany(CpuCooler)

RamType.hasMany(Motherboard)
RamType.hasMany(Ram)

SsdInterface.hasMany(Motherboard)
SsdInterface.hasMany(Ssd)

Bus.hasMany(Motherboard)
Bus.hasMany(Gpu)

Cpu.belongsTo(Socket)
Cpu.belongsTo(Manufacturer)
Cpu.hasMany(Assembly)

CpuCooler.belongsTo(Socket)
CpuCooler.belongsTo(Manufacturer)
CpuCooler.hasMany(Assembly)

Ram.belongsTo(RamType)
Ram.belongsTo(Manufacturer)
Ram.hasMany(Assembly)

Gpu.belongsTo(Bus)
Gpu.belongsTo(Manufacturer)
Gpu.hasMany(Assembly)

Ssd.belongsTo(SsdInterface)
Ssd.belongsTo(Manufacturer)
Ssd.hasMany(Assembly)

PowerSupply.belongsTo(Manufacturer)
PowerSupply.hasMany(Assembly)

CaseFan.belongsTo(FanSize)
CaseFan.belongsTo(Manufacturer)
CaseFan.hasMany(Assembly)

Assembly.belongsTo(User)
Assembly.belongsTo(Case)
Assembly.belongsTo(Motherboard)

Assembly.belongsTo(Cpu)
Assembly.belongsTo(CpuCooler)
Assembly.belongsTo(Ram)
Assembly.belongsTo(Gpu)
Assembly.belongsTo(Ssd)
Assembly.belongsTo(PowerSupply)
Assembly.belongsTo(CaseFan)
Assembly.hasMany(Like)

Like.belongsTo(User)
Like.belongsTo(Assembly)

module.exports = {
    sequelize,
    Sequelize,
    User,
    Case,
    FormFactor,
    Manufacturer,
    FanSize,
    Motherboard,
    Socket,
    RamType,
    SsdInterface,
    Bus,
    Cpu,
    CpuCooler,
    Ram,
    Gpu,
    Ssd,
    PowerSupply,
    CaseFan,
    Assembly,
    Like
}