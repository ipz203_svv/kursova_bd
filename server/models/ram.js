module.exports = (sequelize, DataTypes) => {
    const Ram = sequelize.define('ram', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        size: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'size'
        },
        clock: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'clock'
        },
        lowProfile: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            field: 'low_profile'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'rams'});

    return Ram;
};
