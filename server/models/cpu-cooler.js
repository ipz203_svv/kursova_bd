'use strict';

module.exports = (sequelize, DataTypes) => {
    const CpuCooler = sequelize.define('cpuCooler', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        height: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'height'
        },
        tdp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'tdp'
        },
        lowProfileRam: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            field: 'low_profile_ram'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'cpu_coolers'});

    return CpuCooler;
};
