module.exports = (sequelize, DataTypes) => {
    const Ssd = sequelize.define('ssd', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        size: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'size'
        },
        readSpeed: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'read_speed'
        },
        writeSpeed: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'write_speed'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'ssds'});

    return Ssd;
};
