module.exports = (sequelize, DataTypes) => {
    const Motherboard = sequelize.define('motherboard', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        chipset: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'chipset'
        },
        ramCount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'ram_count'
        },
        ramMaxCapacity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'ram_max_capacity'
        },
        m2Count: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'm2_count'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'motherboards'});

    return Motherboard;
};
