module.exports = (sequelize, DataTypes) => {
    const Case = sequelize.define('case', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        width: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'width'
        },
        height: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'height'
        },
        innerLength: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'inner_length'
        },
        depth: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'depth'
        },
        maxPsuLength: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'max_psu_length'
        },
        fanCount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'fan_count'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'cases'});

    return Case;
};
