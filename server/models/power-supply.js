module.exports = (sequelize, DataTypes) => {
    const PowerSupply = sequelize.define('powerSupply', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            field: 'name'
        },
        releaseYear: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'release_year'
        },
        maxPower: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'max_power'
        },
        rating80Plus: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'rating_80_plus'
        },
        psuLength: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'psu_length'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {tableName: 'power_supplies'});

    return PowerSupply;
};
