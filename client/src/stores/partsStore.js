import {makeAutoObservable} from "mobx";

export default class PartsStore {
    constructor() {
        this._caseFans = []
        this._cases = []
        this._cpuCoolers = []
        this._cpus_ = []
        this._gpus = []
        this._motherboards = []
        this._powerSupplies = []
        this._rams = []
        this._ssds = []

        this._manufacturers = []
        this._sockets_ = []
        this._buses = []
        this._formFactors = []
        this._ramTypes = []
        this._ssdInterfaces = []
        this._fanSizes = []

        this._assemblies = []

        makeAutoObservable(this)
    }

    get assemblies() {
        return this._assemblies;
    }

    set assemblies(value) {
        this._assemblies = value;
    }

    get cpus_() {
        return this._cpus_;
    }

    set cpus_(value) {
        this._cpus_ = value;
    }

    get fanSizes() {
        return this._fanSizes;
    }

    set fanSizes(value) {
        this._fanSizes = value;
    }

    get ssdInterfaces() {
        return this._ssdInterfaces;
    }

    set ssdInterfaces(value) {
        this._ssdInterfaces = value;
    }

    get ramTypes() {
        return this._ramTypes;
    }

    set ramTypes(value) {
        this._ramTypes = value;
    }

    get formFactors() {
        return this._formFactors;
    }

    set formFactors(value) {
        this._formFactors = value;
    }

    get buses() {
        return this._buses;
    }

    set buses(value) {
        this._buses = value;
    }

    get sockets_() {
        return this._sockets_;
    }

    set sockets_(value) {
        this._sockets_ = value;
    }

    get manufacturers() {
        return this._manufacturers;
    }

    set manufacturers(value) {
        this._manufacturers = value;
    }


    get ssds() {
        return this._ssds;
    }

    set ssds(value) {
        this._ssds = value;
    }

    get rams() {
        return this._rams;
    }

    set rams(value) {
        this._rams = value;
    }

    get powerSupplies() {
        return this._powerSupplies;
    }

    set powerSupplies(value) {
        this._powerSupplies = value;
    }

    get motherboards() {
        return this._motherboards;
    }

    set motherboards(value) {
        this._motherboards = value;
    }

    get gpus() {
        return this._gpus;
    }

    set gpus(value) {
        this._gpus = value;
    }

    get cpuCoolers() {
        return this._cpuCoolers;
    }

    set cpuCoolers(value) {
        this._cpuCoolers = value;
    }

    get cases() {
        return this._cases;
    }

    set cases(value) {
        this._cases = value;
    }

    get caseFans() {
        return this._caseFans;
    }

    set caseFans(value) {
        this._caseFans = value;
    }
}
