import {makeAutoObservable} from "mobx";

export default class PaginationStore {
    constructor() {
        this._cpuTotalCount = 0
        this._cpuLimit = 10
        this._cpuActivePage = 1

        this._gpuTotalCount = 0
        this._gpuLimit = 10
        this._gpuActivePage = 1

        this._caseFanTotalCount = 0
        this._caseFanLimit = 10
        this._caseFanActivePage = 1

        this._caseTotalCount = 0
        this._caseLimit = 10
        this._caseActivePage = 1

        this._cpuCoolerTotalCount = 0
        this._cpuCoolerLimit = 10
        this._cpuCoolerActivePage = 1

        this._motherboardTotalCount = 0
        this._motherboardLimit = 10
        this._motherboardActivePage = 1

        this._powerSupplyTotalCount = 0
        this._powerSupplyLimit = 10
        this._powerSupplyActivePage = 1

        this._ramTotalCount = 0
        this._ramLimit = 10
        this._ramActivePage = 1

        this._ssdTotalCount = 0
        this._ssdLimit = 10
        this._ssdActivePage = 1

        this._assemblyTotalCount = 0
        this._assemblyLimit = 10
        this._assemblyActivePage = 1

        makeAutoObservable(this)
    }

    get assemblyActivePage() {
        return this._assemblyActivePage;
    }

    set assemblyActivePage(value) {
        this._assemblyActivePage = value;
    }

    get assemblyLimit() {
        return this._assemblyLimit;
    }

    set assemblyLimit(value) {
        this._assemblyLimit = value;
    }

    get assemblyTotalCount() {
        return this._assemblyTotalCount;
    }

    set assemblyTotalCount(value) {
        this._assemblyTotalCount = value;
    }

    get ssdActivePage() {
        return this._ssdActivePage;
    }

    set ssdActivePage(value) {
        this._ssdActivePage = value;
    }

    get ssdLimit() {
        return this._ssdLimit;
    }

    set ssdLimit(value) {
        this._ssdLimit = value;
    }

    get ssdTotalCount() {
        return this._ssdTotalCount;
    }

    set ssdTotalCount(value) {
        this._ssdTotalCount = value;
    }

    get ramActivePage() {
        return this._ramActivePage;
    }

    set ramActivePage(value) {
        this._ramActivePage = value;
    }

    get ramLimit() {
        return this._ramLimit;
    }

    set ramLimit(value) {
        this._ramLimit = value;
    }

    get ramTotalCount() {
        return this._ramTotalCount;
    }

    set ramTotalCount(value) {
        this._ramTotalCount = value;
    }

    get powerSupplyActivePage() {
        return this._powerSupplyActivePage;
    }

    set powerSupplyActivePage(value) {
        this._powerSupplyActivePage = value;
    }

    get powerSupplyLimit() {
        return this._powerSupplyLimit;
    }

    set powerSupplyLimit(value) {
        this._powerSupplyLimit = value;
    }

    get powerSupplyTotalCount() {
        return this._powerSupplyTotalCount;
    }

    set powerSupplyTotalCount(value) {
        this._powerSupplyTotalCount = value;
    }

    get motherboardActivePage() {
        return this._motherboardActivePage;
    }

    set motherboardActivePage(value) {
        this._motherboardActivePage = value;
    }

    get motherboardLimit() {
        return this._motherboardLimit;
    }

    set motherboardLimit(value) {
        this._motherboardLimit = value;
    }

    get motherboardTotalCount() {
        return this._motherboardTotalCount;
    }

    set motherboardTotalCount(value) {
        this._motherboardTotalCount = value;
    }

    get cpuCoolerActivePage() {
        return this._cpuCoolerActivePage;
    }

    set cpuCoolerActivePage(value) {
        this._cpuCoolerActivePage = value;
    }

    get cpuCoolerLimit() {
        return this._cpuCoolerLimit;
    }

    set cpuCoolerLimit(value) {
        this._cpuCoolerLimit = value;
    }

    get cpuCoolerTotalCount() {
        return this._cpuCoolerTotalCount;
    }

    set cpuCoolerTotalCount(value) {
        this._cpuCoolerTotalCount = value;
    }

    get caseActivePage() {
        return this._caseActivePage;
    }

    set caseActivePage(value) {
        this._caseActivePage = value;
    }

    get caseLimit() {
        return this._caseLimit;
    }

    set caseLimit(value) {
        this._caseLimit = value;
    }

    get caseTotalCount() {
        return this._caseTotalCount;
    }

    set caseTotalCount(value) {
        this._caseTotalCount = value;
    }

    get caseFanActivePage() {
        return this._caseFanActivePage;
    }

    set caseFanActivePage(value) {
        this._caseFanActivePage = value;
    }

    get caseFanLimit() {
        return this._caseFanLimit;
    }

    set caseFanLimit(value) {
        this._caseFanLimit = value;
    }

    get caseFanTotalCount() {
        return this._caseFanTotalCount;
    }

    set caseFanTotalCount(value) {
        this._caseFanTotalCount = value;
    }

    get gpuActivePage() {
        return this._gpuActivePage;
    }

    set gpuActivePage(value) {
        this._gpuActivePage = value;
    }

    get gpuLimit() {
        return this._gpuLimit;
    }

    set gpuLimit(value) {
        this._gpuLimit = value;
    }

    get gpuTotalCount() {
        return this._gpuTotalCount;
    }

    set gpuTotalCount(value) {
        this._gpuTotalCount = value;
    }

    get cpuActivePage() {
        return this._cpuActivePage;
    }

    set cpuActivePage(value) {
        this._cpuActivePage = value;
    }

    get cpuLimit() {
        return this._cpuLimit;
    }

    set cpuLimit(value) {
        this._cpuLimit = value;
    }

    get cpuTotalCount() {
        return this._cpuTotalCount;
    }

    set cpuTotalCount(value) {
        this._cpuTotalCount = value;
    }
}