import {makeAutoObservable} from "mobx";

export default class AssemblyStore {
    constructor() {
        this._caseFan = null
        this._caseFanCount = 0
        this._case_ = null
        this._cpuCooler = null
        this._cpu_ = null
        this._gpu = null
        this._motherboard = null
        this._powerSupply = null
        this._ram = null
        this._ramCount = 0
        this._ssd = null

        this._messages = []

        makeAutoObservable(this)
    }

    get messages() {
        return this._messages;
    }

    set messages(value) {
        this._messages = value;
    }

    get ssd() {
        return this._ssd;
    }

    set ssd(value) {
        this._ssd = value;
    }

    get ramCount() {
        return this._ramCount;
    }

    set ramCount(value) {
        this._ramCount = value;
    }

    get ram() {
        return this._ram;
    }

    set ram(value) {
        this._ram = value;
    }

    get powerSupply() {
        return this._powerSupply;
    }

    set powerSupply(value) {
        this._powerSupply = value;
    }

    get motherboard() {
        return this._motherboard;
    }

    set motherboard(value) {
        this._motherboard = value;
    }

    get gpu() {
        return this._gpu;
    }

    set gpu(value) {
        this._gpu = value;
    }

    get cpu_() {
        return this._cpu_;
    }

    set cpu_(value) {
        this._cpu_ = value;
    }

    get cpuCooler() {
        return this._cpuCooler;
    }

    set cpuCooler(value) {
        this._cpuCooler = value;
    }

    get case_() {
        return this._case_;
    }

    set case_(value) {
        this._case_ = value;
    }

    get caseFanCount() {
        return this._caseFanCount;
    }

    set caseFanCount(value) {
        this._caseFanCount = value;
    }

    get caseFan() {
        return this._caseFan;
    }

    set caseFan(value) {
        this._caseFan = value;
    }
}