import {makeAutoObservable} from "mobx";

export default class FilteringStore {
    constructor() {
        this._cpuSelectedManufacturer = {}
        this._cpuSelectedSocket = {}
        this._cpuSelectedSocketDisabled = false

        this._gpuSelectedManufacturer = {}
        this._gpuSelectedBus = {}
        this._gpuSelectedBusDisabled = false

        this._caseFanSelectedManufacturer = {}
        this._caseFanSelectedFanSize = {}
        this._caseFanSelectedFanSizeDisabled = false

        this._caseSelectedManufacturer = {}
        this._caseSelectedFormFactor = {}
        this._caseSelectedFormFactorDisabled = false
        this._caseSelectedFanSize = {}
        this._caseSelectedFanSizeDisabled = false

        this._cpuCoolerSelectedManufacturer = {}
        this._cpuCoolerSelectedSocket = {}
        this._cpuCoolerSelectedSocketDisabled = false

        this._motherboardSelectedManufacturer = {}
        this._motherboardSelectedSocket = {}
        this._motherboardSelectedSocketDisabled = false
        this._motherboardSelectedFormFactor = {}
        this._motherboardSelectedFormFactorDisabled = false
        this._motherboardSelectedRamType = {}
        this._motherboardSelectedRamTypeDisabled = false
        this._motherboardSelectedSsdInterface = {}
        this._motherboardSelectedSsdInterfaceDisabled = false
        this._motherboardSelectedBus = {}
        this._motherboardSelectedBusDisabled = false

        this._powerSupplySelectedManufacturer = {}

        this._ramSelectedManufacturer = {}
        this._ramSelectedRamType = {}
        this._ramSelectedRamTypeDisabled = false

        this._ssdSelectedManufacturer = {}
        this._ssdSelectedSsdInterface = {}
        this._ssdSelectedSsdInterfaceDisabled = false

        this._assembliesSortMethod = 'created_at'

        makeAutoObservable(this)
    }

    get assembliesSortMethod() {
        return this._assembliesSortMethod;
    }

    set assembliesSortMethod(value) {
        this._assembliesSortMethod = value;
    }

    get ssdSelectedSsdInterfaceDisabled() {
        return this._ssdSelectedSsdInterfaceDisabled;
    }

    set ssdSelectedSsdInterfaceDisabled(value) {
        this._ssdSelectedSsdInterfaceDisabled = value;
    }

    get ramSelectedRamTypeDisabled() {
        return this._ramSelectedRamTypeDisabled;
    }

    set ramSelectedRamTypeDisabled(value) {
        this._ramSelectedRamTypeDisabled = value;
    }

    get motherboardSelectedBusDisabled() {
        return this._motherboardSelectedBusDisabled;
    }

    set motherboardSelectedBusDisabled(value) {
        this._motherboardSelectedBusDisabled = value;
    }

    get motherboardSelectedSsdInterfaceDisabled() {
        return this._motherboardSelectedSsdInterfaceDisabled;
    }

    set motherboardSelectedSsdInterfaceDisabled(value) {
        this._motherboardSelectedSsdInterfaceDisabled = value;
    }

    get motherboardSelectedRamTypeDisabled() {
        return this._motherboardSelectedRamTypeDisabled;
    }

    set motherboardSelectedRamTypeDisabled(value) {
        this._motherboardSelectedRamTypeDisabled = value;
    }

    get motherboardSelectedFormFactorDisabled() {
        return this._motherboardSelectedFormFactorDisabled;
    }

    set motherboardSelectedFormFactorDisabled(value) {
        this._motherboardSelectedFormFactorDisabled = value;
    }

    get motherboardSelectedSocketDisabled() {
        return this._motherboardSelectedSocketDisabled;
    }

    set motherboardSelectedSocketDisabled(value) {
        this._motherboardSelectedSocketDisabled = value;
    }

    get cpuCoolerSelectedSocketDisabled() {
        return this._cpuCoolerSelectedSocketDisabled;
    }

    set cpuCoolerSelectedSocketDisabled(value) {
        this._cpuCoolerSelectedSocketDisabled = value;
    }

    get caseSelectedFanSizeDisabled() {
        return this._caseSelectedFanSizeDisabled;
    }

    set caseSelectedFanSizeDisabled(value) {
        this._caseSelectedFanSizeDisabled = value;
    }

    get caseSelectedFormFactorDisabled() {
        return this._caseSelectedFormFactorDisabled;
    }

    set caseSelectedFormFactorDisabled(value) {
        this._caseSelectedFormFactorDisabled = value;
    }

    get caseFanSelectedFanSizeDisabled() {
        return this._caseFanSelectedFanSizeDisabled;
    }

    set caseFanSelectedFanSizeDisabled(value) {
        this._caseFanSelectedFanSizeDisabled = value;
    }

    get gpuSelectedBusDisabled() {
        return this._gpuSelectedBusDisabled;
    }

    set gpuSelectedBusDisabled(value) {
        this._gpuSelectedBusDisabled = value;
    }

    get cpuSelectedSocketDisabled() {
        return this._cpuSelectedSocketDisabled;
    }

    set cpuSelectedSocketDisabled(value) {
        this._cpuSelectedSocketDisabled = value;
    }

    get ssdSelectedSsdInterface() {
        return this._ssdSelectedSsdInterface;
    }

    set ssdSelectedSsdInterface(value) {
        this._ssdSelectedSsdInterface = value;
    }

    get ssdSelectedManufacturer() {
        return this._ssdSelectedManufacturer;
    }

    set ssdSelectedManufacturer(value) {
        this._ssdSelectedManufacturer = value;
    }

    get ramSelectedRamType() {
        return this._ramSelectedRamType;
    }

    set ramSelectedRamType(value) {
        this._ramSelectedRamType = value;
    }

    get ramSelectedManufacturer() {
        return this._ramSelectedManufacturer;
    }

    set ramSelectedManufacturer(value) {
        this._ramSelectedManufacturer = value;
    }

    get powerSupplySelectedManufacturer() {
        return this._powerSupplySelectedManufacturer;
    }

    set powerSupplySelectedManufacturer(value) {
        this._powerSupplySelectedManufacturer = value;
    }

    get motherboardSelectedBus() {
        return this._motherboardSelectedBus;
    }

    set motherboardSelectedBus(value) {
        this._motherboardSelectedBus = value;
    }

    get motherboardSelectedSsdInterface() {
        return this._motherboardSelectedSsdInterface;
    }

    set motherboardSelectedSsdInterface(value) {
        this._motherboardSelectedSsdInterface = value;
    }

    get motherboardSelectedRamType() {
        return this._motherboardSelectedRamType;
    }

    set motherboardSelectedRamType(value) {
        this._motherboardSelectedRamType = value;
    }

    get motherboardSelectedFormFactor() {
        return this._motherboardSelectedFormFactor;
    }

    set motherboardSelectedFormFactor(value) {
        this._motherboardSelectedFormFactor = value;
    }

    get motherboardSelectedSocket() {
        return this._motherboardSelectedSocket;
    }

    set motherboardSelectedSocket(value) {
        this._motherboardSelectedSocket = value;
    }

    get motherboardSelectedManufacturer() {
        return this._motherboardSelectedManufacturer;
    }

    set motherboardSelectedManufacturer(value) {
        this._motherboardSelectedManufacturer = value;
    }

    get cpuCoolerSelectedSocket() {
        return this._cpuCoolerSelectedSocket;
    }

    set cpuCoolerSelectedSocket(value) {
        this._cpuCoolerSelectedSocket = value;
    }

    get cpuCoolerSelectedManufacturer() {
        return this._cpuCoolerSelectedManufacturer;
    }

    set cpuCoolerSelectedManufacturer(value) {
        this._cpuCoolerSelectedManufacturer = value;
    }

    get caseSelectedFanSize() {
        return this._caseSelectedFanSize;
    }

    set caseSelectedFanSize(value) {
        this._caseSelectedFanSize = value;
    }

    get caseSelectedFormFactor() {
        return this._caseSelectedFormFactor;
    }

    set caseSelectedFormFactor(value) {
        this._caseSelectedFormFactor = value;
    }

    get caseSelectedManufacturer() {
        return this._caseSelectedManufacturer;
    }

    set caseSelectedManufacturer(value) {
        this._caseSelectedManufacturer = value;
    }

    get caseFanSelectedFanSize() {
        return this._caseFanSelectedFanSize;
    }

    set caseFanSelectedFanSize(value) {
        this._caseFanSelectedFanSize = value;
    }

    get caseFanSelectedManufacturer() {
        return this._caseFanSelectedManufacturer;
    }

    set caseFanSelectedManufacturer(value) {
        this._caseFanSelectedManufacturer = value;
    }

    get gpuSelectedBus() {
        return this._gpuSelectedBus;
    }

    set gpuSelectedBus(value) {
        this._gpuSelectedBus = value;
    }

    get gpuSelectedManufacturer() {
        return this._gpuSelectedManufacturer;
    }

    set gpuSelectedManufacturer(value) {
        this._gpuSelectedManufacturer = value;
    }

    get cpuSelectedSocket() {
        return this._cpuSelectedSocket;
    }

    set cpuSelectedSocket(value) {
        this._cpuSelectedSocket = value;
    }

    get cpuSelectedManufacturer() {
        return this._cpuSelectedManufacturer;
    }

    set cpuSelectedManufacturer(value) {
        this._cpuSelectedManufacturer = value;
    }
}