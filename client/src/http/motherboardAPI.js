import {$authHost, $host} from "./index";

export const createMotherboard = async (motherboard) => {
    const {data} = await $authHost.post('api/motherboard', motherboard)
    return data
}

export const fetchMotherboards = async (name, formFactorId, manufacturerId, socketId, ramTypeId, ssdInterfaceId, busId, page, limit = 10) => {
    const {data} = await $host.get('api/motherboard', {
        params: {
            name, formFactorId, manufacturerId, socketId, ramTypeId, ssdInterfaceId, busId, page, limit
        }
    })
    return data
}

export const fetchMotherboard = async (id) => {
    const {data} = await $host.get('api/motherboard/' + id)
    return data
}

export const deleteMotherboard = async (id) => {
    const {data} = await $authHost.delete('api/motherboard/' + id)
    return data
}