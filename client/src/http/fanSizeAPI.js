import {$authHost, $host} from "./index";

export const createFanSize = async (fanSize) => {
    const {data} = await $authHost.post('api/fan-size', fanSize)
    return data
}

export const fetchFanSizes = async () => {
    const {data} = await $host.get('api/fan-size')
    return data
}

export const fetchFanSize = async (id) => {
    const {data} = await $host.get('api/fan-size/' + id)
    return data
}

export const deleteFanSize = async (id) => {
    const {data} = await $authHost.delete('api/fan-size/' + id)
    return data
}