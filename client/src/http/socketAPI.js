import {$authHost, $host} from "./index";

export const createSocket = async (socket) => {
    const {data} = await $authHost.post('api/socket', socket)
    return data
}

export const fetchSockets = async () => {
    const {data} = await $host.get('api/socket')
    return data
}

export const fetchSocket = async (id) => {
    const {data} = await $host.get('api/socket/' + id)
    return data
}

export const deleteSocket = async (id) => {
    const {data} = await $authHost.delete('api/socket/' + id)
    return data
}