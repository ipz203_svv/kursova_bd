import {$authHost, $host} from "./index";

export const createLike = async (like) => {
    const {data} = await $authHost.post('api/like', like)
    return data
}

export const fetchLikesCount = async (id) => {
    const {data} = await $host.get('api/like/' + id)
    return data
}

export const fetchLike = async (userId, assemblyId) => {
    const {data} = await $host.get('api/like', {
        params: {
            userId, assemblyId
        }
    })
    return data
}

export const deleteLike = async (id) => {
    const {data} = await $authHost.delete('api/like/' + id)
    return data
}