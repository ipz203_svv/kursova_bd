import {$authHost, $host} from "./index";

export const createRam = async (ram) => {
    const {data} = await $authHost.post('api/ram', ram)
    return data
}

export const fetchRams = async (name, manufacturerId, ramTypeId, page, limit = 10) => {
    const {data} = await $host.get('api/ram', {
        params: {
            name, manufacturerId, ramTypeId, page, limit
        }
    })
    return data
}

export const fetchRam = async (id) => {
    const {data} = await $host.get('api/ram/' + id)
    return data
}

export const deleteRam = async (id) => {
    const {data} = await $authHost.delete('api/ram/' + id)
    return data
}