import {$authHost, $host} from "./index";

export const createRamType = async (socket) => {
    const {data} = await $authHost.post('api/ram-type', socket)
    return data
}

export const fetchRamTypes = async () => {
    const {data} = await $host.get('api/ram-type')
    return data
}

export const fetchRamType = async (id) => {
    const {data} = await $host.get('api/ram-type/' + id)
    return data
}

export const deleteRamType = async (id) => {
    const {data} = await $authHost.delete('api/ram-type/' + id)
    return data
}