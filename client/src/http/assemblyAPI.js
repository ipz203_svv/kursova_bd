import {$authHost, $host} from "./index";

export const createAssembly = async (assembly) => {
    const {data} = await $authHost.post('api/assembly', assembly)
    return data
}

export const fetchAssemblies = async (userId, sort, page, limit = 10) => {
    const {data} = await $host.get('api/assembly', {
        params: {
            userId, sort, page, limit
        }
    })
    return data
}

export const fetchAssembly = async (id) => {
    const {data} = await $host.get('api/assembly/' + id)
    return data
}

export const deleteAssembly = async (id) => {
    const {data} = await $authHost.delete('api/assembly/' + id)
    return data
}