import {$authHost, $host} from "./index";

export const createPowerSupply = async (powerSupply) => {
    const {data} = await $authHost.post('api/power-supply', powerSupply)
    return data
}

export const fetchPowerSupplies = async (name, manufacturerId, page, limit = 10) => {
    const {data} = await $host.get('api/power-supply', {
        params: {
            name, manufacturerId, page, limit
        }
    })
    return data
}

export const fetchPowerSupply = async (id) => {
    const {data} = await $host.get('api/power-supply/' + id)
    return data
}

export const deletePowerSupply = async (id) => {
    const {data} = await $authHost.delete('api/power-supply/' + id)
    return data
}