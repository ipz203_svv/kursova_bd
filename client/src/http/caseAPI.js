import {$authHost, $host} from "./index";

export const createCase = async (case_) => {
    const {data} = await $authHost.post('api/case', case_)
    return data
}

export const fetchCases = async (name, manufacturerId, formFactorId, fanSizeId, page, limit = 10) => {
    const {data} = await $host.get('api/case', {
        params: {
            name, formFactorId, manufacturerId, fanSizeId, page, limit
        }
    })
    return data
}

export const fetchCase = async (id) => {
    const {data} = await $host.get('api/case/' + id)
    return data
}

export const deleteCase = async (id) => {
    const {data} = await $authHost.delete('api/case/' + id)
    return data
}