import {$authHost, $host} from "./index";

export const createSsdInterface = async (ssdInterface) => {
    const {data} = await $authHost.post('api/ssd-interface', ssdInterface)
    return data
}

export const fetchSsdInterfaces = async () => {
    const {data} = await $host.get('api/ssd-interface')
    return data
}

export const fetchSsdInterface = async (id) => {
    const {data} = await $host.get('api/ssd-interface/' + id)
    return data
}

export const deleteSsdInterface = async (id) => {
    const {data} = await $authHost.delete('api/ssd-interface/' + id)
    return data
}