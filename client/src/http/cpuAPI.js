import {$authHost, $host} from "./index";

export const createCpu = async (cpu) => {
    const {data} = await $authHost.post('api/cpu', cpu)
    return data
}

export const fetchCpus = async (name, manufacturerId, socketId, page, limit = 10) => {
    const {data} = await $host.get('api/cpu', {
        params: {
            name, manufacturerId, socketId, page, limit
        }
    })
    return data
}

export const fetchCpu = async (id) => {
    const {data} = await $host.get('api/cpu/' + id)
    return data
}

export const deleteCpu = async (id) => {
    const {data} = await $authHost.delete('api/cpu/' + id)
    return data
}