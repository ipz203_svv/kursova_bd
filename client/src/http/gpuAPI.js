import {$authHost, $host} from "./index";

export const createGpu = async (gpu) => {
    const {data} = await $authHost.post('api/gpu', gpu)
    return data
}

export const fetchGpus = async (name, manufacturerId, busId, page, limit = 10) => {
    const {data} = await $host.get('api/gpu', {
        params: {
            name, manufacturerId, busId, page, limit
        }
    })
    return data
}

export const fetchGpu = async (id) => {
    const {data} = await $host.get('api/gpu/' + id)
    return data
}

export const deleteGpu = async (id) => {
    const {data} = await $authHost.delete('api/gpu/' + id)
    return data
}