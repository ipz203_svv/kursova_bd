import {$authHost, $host} from "./index";

export const createBus = async (bus) => {
    const {data} = await $authHost.post('api/bus', bus)
    return data
}

export const fetchBuses = async () => {
    const {data} = await $host.get('api/bus')
    return data
}

export const fetchBus = async (id) => {
    const {data} = await $host.get('api/bus/' + id)
    return data
}

export const deleteBus = async (id) => {
    const {data} = await $authHost.delete('api/bus/' + id)
    return data
}