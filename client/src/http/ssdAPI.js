import {$authHost, $host} from "./index";

export const createSsd = async (ssd) => {
    const {data} = await $authHost.post('api/ssd', ssd)
    return data
}

export const fetchSsds = async (name, manufacturerId, ssdInterfaceId, page, limit = 10) => {
    const {data} = await $host.get('api/ssd', {
        params: {
            name, manufacturerId, ssdInterfaceId, page, limit
        }
    })
    return data
}

export const fetchSsd = async (id) => {
    const {data} = await $host.get('api/ssd/' + id)
    return data
}

export const deleteSsd = async (id) => {
    const {data} = await $authHost.delete('api/sdd/' + id)
    return data
}