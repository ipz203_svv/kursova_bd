import {$authHost, $host} from "./index";

export const createCaseFan = async (caseFan) => {
    const {data} = await $authHost.post('api/case-fan', caseFan)
    return data
}

export const fetchCaseFans = async (name, manufacturerId, fanSizeId, page, limit = 10) => {
    const {data} = await $host.get('api/case-fan', {
        params: {
            name, manufacturerId, fanSizeId, page, limit
        }
    })
    return data
}

export const fetchCaseFan = async (id) => {
    const {data} = await $host.get('api/case-fan/' + id)
    return data
}

export const deleteCaseFan = async (id) => {
    const {data} = await $authHost.delete('api/case-fan/' + id)
    return data
}