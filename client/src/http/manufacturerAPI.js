import {$authHost, $host} from "./index";

export const createManufacturer = async (manufacturer) => {
    const {data} = await $authHost.post('api/manufacturer', manufacturer)
    return data
}

export const fetchManufacturers = async () => {
    const {data} = await $host.get('api/manufacturer')
    return data
}

export const fetchManufacturer = async (id) => {
    const {data} = await $host.get('api/manufacturer/' + id)
    return data
}

export const deleteManufacturer = async (id) => {
    const {data} = await $authHost.delete('api/manufacturer/' + id)
    return data
}