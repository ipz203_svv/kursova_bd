import {$authHost, $host} from "./index";

export const createCpuCooler = async (cpuCooler) => {
    const {data} = await $authHost.post('api/cpu-cooler', cpuCooler)
    return data
}

export const fetchCpuCoolers = async (name, manufacturerId, socketId, page, limit = 10) => {
    const {data} = await $host.get('api/cpu-cooler', {
        params: {
            name, manufacturerId, socketId, page, limit
        }
    })
    return data
}

export const fetchCpuCooler = async (id) => {
    const {data} = await $host.get('api/cpu-cooler/' + id)
    return data
}

export const deleteCpuCooler = async (id) => {
    const {data} = await $authHost.delete('api/cpu-cooler/' + id)
    return data
}