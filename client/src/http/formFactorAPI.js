import {$authHost, $host} from "./index";

export const createFormFactor = async (formFactor) => {
    const {data} = await $authHost.post('api/form-factor', formFactor)
    return data
}

export const fetchFormFactors = async () => {
    const {data} = await $host.get('api/form-factor')
    return data
}

export const fetchFormFactor = async (id) => {
    const {data} = await $host.get('api/form-factor/' + id)
    return data
}

export const deleteFormFactor = async (id) => {
    const {data} = await $authHost.delete('api/form-factor/' + id)
    return data
}