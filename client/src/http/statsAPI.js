import {$authHost} from "./index";

export const GetUsers = async (month) => {
    const {data} = await $authHost.get('api/stats/users', {params: {month}})
    return data
}

export const GetAssemblies = async (month) => {
    const {data} = await $authHost.get('api/stats/assemblies', {params: {month}})
    return data
}




