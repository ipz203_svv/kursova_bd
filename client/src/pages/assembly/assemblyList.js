import {observer} from "mobx-react-lite";
import React, {useContext, useEffect} from 'react';
import {Dropdown} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import AssemblyComponent from "../../components/assemblyComponent";
import PaginatorAssembly from "../../components/paginators/paginatorAssembly";
import {fetchAssemblies} from "../../http/assemblyAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchRamTypes} from "../../http/ramTypeAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT} from "../../utils/consts";

const AssemblyList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering} = useContext(Context)

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        filtering.assembliesSortMethod = 'created_at'
        fetchAssemblies(null, filtering.assembliesSortMethod, 1, 5).then(data => {
            parts.assemblies = data.rows
        })
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchRamTypes().then(data => parts.ramTypes = data)
    }, [])

    useEffect(() => {
        fetchAssemblies(null, filtering.assembliesSortMethod, pagination.assemblyActivePage, limit).then(data => {
            parts.assemblies = data.rows
            pagination.assemblyTotalCount = data.count
            pagination.assemblyLimit = limit
            if (pagination.assemblyTotalCount / pagination.assemblyLimit > pagination.assemblyActivePage) {
                pagination.assemblyActivePag = pagination.assemblyTotalCount / pagination.assemblyLimit
            }
        })
    }, [pagination.assemblyActivePage, filtering.assembliesSortMethod])

    return (<div className="container">
        <div className="display-3">Збірки</div>
        <div className="d-flex flex-row-reverse">
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.assembliesSortMethod === 'created_at' ? 'Нові' : 'Популярні'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.assembliesSortMethod = 'created_at'}
                    >Нові</Dropdown.Item>
                    <Dropdown.Item
                        key={1}
                        onClick={() => filtering.assembliesSortMethod = 'likesCount'}
                    >Популярні</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        {parts.assemblies.map(assembly => <AssemblyComponent key={assembly.id} assembly={assembly}/>)}
        <PaginatorAssembly/>
    </div>);
});

export default AssemblyList;