import React, {useContext, useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useNavigate, useParams} from "react-router-dom";
import {fetchAssembly} from "../../http/assemblyAPI";
import {fetchCase} from "../../http/caseAPI";
import {fetchCaseFan} from "../../http/caseFanAPI";
import {fetchCpu} from "../../http/cpuAPI";
import {fetchCpuCooler} from "../../http/cpuCoolerAPI";
import {fetchGpu} from "../../http/gpuAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchMotherboard} from "../../http/motherboardAPI";
import {fetchPowerSupply} from "../../http/powerSupplyAPI";
import {fetchRam} from "../../http/ramAPI";
import {fetchSsd} from "../../http/ssdAPI";
import {Context} from "../../index";
import {
    CASE_FAN_ROUTE,
    CASE_ROUTE,
    CPU_COOLER_ROUTE,
    CPU_ROUTE,
    GPU_ROUTE,
    MOTHERBOARD_ROUTE,
    POWER_SUPPLY_ROUTE,
    RAM_ROUTE,
    SSD_ROUTE
} from "../../utils/consts";

const Assembly = () => {
    const navigate = useNavigate();
    const {id} = useParams();

    const {parts} = useContext(Context)

    const [assembly, setAssembly] = useState(null);
    const [assemblyLoading, setAssemblyLoading] = useState(true)
    const [ramCount, setRamCount] = useState(0)
    const [caseFanCount, setCaseFanCount] = useState(0)
    const [case_, setCase] = useState(null)
    const [caseLoading, setCaseLoading] = useState(true)
    const [motherboard, setMotherboard] = useState(null)
    const [motherboardLoading, setMotherboardLoading] = useState(true)
    const [cpu, setCpu] = useState(null)
    const [cpuLoading, setCpuLoading] = useState(true)
    const [cpuCooler, setCpuCooler] = useState(null)
    const [cpuCoolerLoading, setCpuCoolerLoading] = useState(true)
    const [ram, setRam] = useState(null)
    const [ramLoading, setRamLoading] = useState(true)
    const [gpu, setGpu] = useState(null)
    const [gpuLoading, setGpuLoading] = useState(true)
    const [ssd, setSsd] = useState(null)
    const [ssdLoading, setSsdLoading] = useState(true)
    const [powerSupply, setPowerSupply] = useState(null)
    const [powerSupplyLoading, setPowerSupplyLoading] = useState(true)
    const [caseFan, setCaseFan] = useState(null)
    const [caseFanLoading, setCaseFanLoading] = useState(true)

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchAssembly(id).then(data => {
            setAssembly(data)
            setAssemblyLoading(false)
        })
    }, [])

    useEffect(() => {
        if (!assemblyLoading) {
            setRamCount(assembly.ramCount)
            setCaseFanCount(assembly.caseFanCount)
            fetchCase(assembly.caseId).then(data => {
                setCase(data)
                setCaseLoading(false)
            })
            fetchMotherboard(assembly.motherboardId).then(data => {
                setMotherboard(data)
                setMotherboardLoading(false)
            })
            fetchCpu(assembly.cpuId).then(data => {
                setCpu(data)
                setCpuLoading(false)
            })
            fetchPowerSupply(assembly.powerSupplyId).then(data => {
                setPowerSupply(data)
                setPowerSupplyLoading(false)
            })
            fetchCpuCooler(assembly.cpuCoolerId).then(data => {
                setCpuCooler(data)
                setCpuCoolerLoading(false)
            })
            fetchGpu(assembly.gpuId).then(data => {
                setGpu(data)
                setGpuLoading(false)
            })
            fetchSsd(assembly.ssdId).then(data => {
                setSsd(data)
                setSsdLoading(false)
            })
            fetchCaseFan(assembly.caseFanId).then(data => {
                setCaseFan(data)
                setCaseFanLoading(false)
            })
            fetchRam(assembly.ramId).then(data => {
                setRam(data)
                setRamLoading(false)
            })
        }
    }, [assembly])

    if (assemblyLoading || caseLoading || motherboardLoading || cpuLoading || cpuCoolerLoading || ramLoading || gpuLoading || ssdLoading || powerSupplyLoading || caseFanLoading) {
        return <div>Loading...</div>
    }

    return (<Container style={{marginTop: 30 + 'px'}}>
        <Row>
            <Col>
                <div>Процесор:</div>
                <div>Кулер процесору:</div>
                <div>Відеокарта:</div>
                <div>Материнська плата:</div>
                <div>Оперативна пам'ять:</div>
                <div>SSD:</div>
                <div>Блок живлення:</div>
                <div>Корпус:</div>
                <div>Кулери корпусу</div>
            </Col>
            <Col>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(CPU_ROUTE + '/' + cpu.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === cpu.manufacturerId).name} {cpu.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(CPU_COOLER_ROUTE + '/' + cpuCooler.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === cpuCooler.manufacturerId).name} {cpuCooler.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(GPU_ROUTE + '/' + gpu.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === gpu.manufacturerId).name} {gpu.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(MOTHERBOARD_ROUTE + '/' + motherboard.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === motherboard.manufacturerId).name} {motherboard.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(RAM_ROUTE + '/' + ram.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === ram.manufacturerId).name} {ram.name} x{ramCount}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(SSD_ROUTE + '/' + ssd.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === ssd.manufacturerId).name} {ssd.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(POWER_SUPPLY_ROUTE + '/' + powerSupply.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === powerSupply.manufacturerId).name} {powerSupply.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(CASE_ROUTE + '/' + case_.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === case_.manufacturerId).name} {case_.name}</div>
                <div className="cursor-pointer link-info"
                     onClick={() => navigate(CASE_FAN_ROUTE + '/' + caseFan.id)}>{parts.manufacturers.find(manufacturer => manufacturer.id === caseFan.manufacturerId).name} {caseFan.name} x{caseFanCount}</div>
            </Col>
        </Row>
    </Container>);
};

export default Assembly;