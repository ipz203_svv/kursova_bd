import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchCpuCooler} from "../../http/cpuCoolerAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchSocket} from "../../http/socketAPI";

const CpuCooler = () => {
    const [cpuCooler, setCpuCooler] = useState({})
    const [socket, setSocket] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchCpuCooler(id).then(data => setCpuCooler(data))
    }, [])

    useEffect(() => {
        fetchSocket(parseInt(cpuCooler.socketId) || 1).then(data => setSocket(data))
        fetchManufacturer(parseInt(cpuCooler.manufacturerId) || 1).then(data => setManufacturer(data))
    }, [cpuCooler])

    return (<Container>
        <div className="display-3">{manufacturer.name} {cpuCooler.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Сокет:</div>
                <div>Висота:</div>
                <div>TDP:</div>
                <div>Потрібна оперативна пам'ять з низьким профілем:</div>
            </Col>
            <Col>
                <div>{cpuCooler.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{cpuCooler.name}</div>
                <div>{socket.name}</div>
                <div>{cpuCooler.height} mm</div>
                <div>{cpuCooler.tdp} W</div>
                <div>{cpuCooler.lowProfileRam ? 'Так' : 'Ні'}</div>
            </Col>
        </Row>
    </Container>);
};

export default CpuCooler;