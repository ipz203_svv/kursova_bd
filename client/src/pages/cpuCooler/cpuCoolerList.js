import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateCpuCooler from "../../components/modals/createCpuCooler";
import PaginatorCpuCooler from "../../components/paginators/paginatorCpuCooler";
import {deleteCpuCooler, fetchCpuCoolers} from "../../http/cpuCoolerAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchSockets} from "../../http/socketAPI";
import {Context} from "../../index";
import {CPU_COOLER_ROUTE, GLOBAL_LIMIT, HOME_ROUTE} from "../../utils/consts";

const CpuCoolerList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [cpuCoolerVisible, setCpuCoolerVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchSockets().then(data => parts.sockets_ = data)
        fetchCpuCoolers(null, null, null, 1, limit).then(data => {
            parts.cpuCoolers = data.rows
            pagination.cpuCoolerTotalCount = data.count
            pagination.cpuCoolerLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchCpuCoolers(searchText, filtering.cpuCoolerSelectedManufacturer.id, filtering.cpuCoolerSelectedSocket.id, pagination.cpuCoolerActivePage, limit).then(data => {
            parts.cpuCoolers = data.rows
            pagination.cpuCoolerTotalCount = data.count
            pagination.cpuCoolerLimit = limit
            if (pagination.cpuCoolerTotalCount / pagination.cpuCoolerLimit > pagination.cpuCoolerActivePage) {
                pagination.cpuCoolerActivePag = pagination.cpuCoolerTotalCount / pagination.cpuCoolerLimit
            }
        })
    }, [pagination.cpuCoolerActivePage, filtering.cpuCoolerSelectedManufacturer, filtering.cpuCoolerSelectedSocket, searchText])

    function handleSelect(cpuCooler) {
        assembly.cpuCooler = cpuCooler
        filtering.cpuSelectedSocket = parts.sockets_.find(socket => socket.id === cpuCooler.socketId)
        filtering.cpuSelectedSocketDisabled = true
        filtering.motherboardSelectedSocket = parts.sockets_.find(socket => socket.id === cpuCooler.socketId)
        filtering.motherboardSelectedSocketDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Кулери процесору</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setCpuCoolerVisible(true)}
            >
                Додати кулер процесору
            </Button>
            <CreateCpuCooler show={cpuCoolerVisible} onHide={() => setCpuCoolerVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.cpuCoolerSelectedSocketDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.cpuCoolerSelectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.sockets_.map(socket => <Dropdown.Item
                        key={socket.id}
                        onClick={() => filtering.cpuCoolerSelectedSocket = socket}
                    >{socket.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.cpuCoolerSelectedSocket = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.cpuCoolerSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.cpuCoolerSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.cpuCoolerSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Сокет</th>
                <th>TDP</th>
            </tr>
            </thead>
            <tbody>
            {parts.cpuCoolers.map(cpuCooler => <tr key={cpuCooler.id}>
                {user.isAdmin() ? <>
                    <td>{cpuCooler.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(cpuCooler)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(CPU_COOLER_ROUTE + '/' + cpuCooler.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteCpuCooler(cpuCooler.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === cpuCooler.manufacturerId).name}</td>
                <td>{cpuCooler.name}</td>
                <td>{parts.sockets_.find(socket => socket.id === cpuCooler.socketId).name}</td>
                <td>{cpuCooler.tdp}</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorCpuCooler/>
    </div>);
});

export default CpuCoolerList;