import React from 'react';
import {Container} from "react-bootstrap";
import HomeAssemblyBuilder from "../components/homeAssemblyBuilder";

const Home = () => {
    return (<Container>
        <HomeAssemblyBuilder/>
    </Container>);
};

export default Home;