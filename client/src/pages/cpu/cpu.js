import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchCpu} from "../../http/cpuAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchSocket} from "../../http/socketAPI";

const Cpu = () => {
    const [cpu, setCpu] = useState({})
    const [socket, setSocket] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchCpu(id).then(data => setCpu(data))
    }, [])

    useEffect(() => {
        fetchSocket(parseInt(cpu.socketId) || 1).then(data => setSocket(data))
        fetchManufacturer(parseInt(cpu.manufacturerId) || 1).then(data => setManufacturer(data))
    }, [cpu])

    return (<Container>
        <div className="display-3">{manufacturer.name} {cpu.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Сокет:</div>
                <div>Ядер:</div>
                <div>Потоків:</div>
                <div>Частота:</div>
                <div>Величина тех процесу:</div>
                <div>Об'єм кешу 3 рівня:</div>
                <div>Максимально підтримувана частота оперативної пам'яті:</div>
                <div>Кіл-сть теплоти, що потрібно розсіювати:</div>
            </Col>
            <Col>
                <div>{cpu.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{cpu.name}</div>
                <div>{socket.name}</div>
                <div>{cpu.core}</div>
                <div>{cpu.thread}</div>
                <div>{cpu.clock} MHz</div>
                <div>{cpu.process} nm</div>
                <div>{cpu.cache} Mb</div>
                <div>{cpu.ramMaxClock} MHz</div>
                <div>{cpu.tdp} W</div>
            </Col>
        </Row>

    </Container>);
};

export default Cpu;