import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateCpu from "../../components/modals/createCpu";
import PaginatorCpu from "../../components/paginators/paginatorCpu";
import {deleteCpu, fetchCpus} from "../../http/cpuAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchSockets} from "../../http/socketAPI";
import {Context} from "../../index";
import {CPU_ROUTE, GLOBAL_LIMIT, HOME_ROUTE} from "../../utils/consts";

const CpuList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [cpuVisible, setCpuVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchSockets().then(data => parts.sockets_ = data)
        fetchCpus(null, null, null, 1, limit).then(data => {
            parts.cpus_ = data.rows
            pagination.cpuTotalCount = data.count
            pagination.cpuLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchCpus(searchText, filtering.cpuSelectedManufacturer.id, filtering.cpuSelectedSocket.id, pagination.cpuActivePage, limit).then(data => {
            parts.cpus_ = data.rows
            pagination.cpuTotalCount = data.count
            pagination.cpuLimit = limit
            if (pagination.cpuTotalCount / pagination.cpuLimit > pagination.cpuActivePage) {
                pagination.cpuActivePag = pagination.cpuTotalCount / pagination.cpuLimit
            }
        })
    }, [pagination.cpuActivePage, filtering.cpuSelectedManufacturer, filtering.cpuSelectedSocket, searchText])

    function handleSelect(cpu_) {
        assembly.cpu_ = cpu_
        filtering.cpuCoolerSelectedSocket = parts.sockets_.find(socket => socket.id === cpu_.socketId)
        filtering.cpuCoolerSelectedSocketDisabled = true
        filtering.motherboardSelectedSocket = parts.sockets_.find(socket => socket.id === cpu_.socketId)
        filtering.motherboardSelectedSocketDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Процесори</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setCpuVisible(true)}
            >
                Додати процесор
            </Button>
            <CreateCpu show={cpuVisible} onHide={() => setCpuVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.cpuSelectedSocketDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.cpuSelectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.sockets_.map(socket => <Dropdown.Item
                        key={socket.id}
                        onClick={() => filtering.cpuSelectedSocket = socket}
                    >{socket.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.cpuSelectedSocket = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.cpuSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.cpuSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.cpuSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Сокет</th>
                <th>Ядер</th>
                <th>Потоків</th>
                <th>Тактова частота</th>
            </tr>
            </thead>
            <tbody>
            {parts.cpus_.map(cpu => <tr key={cpu.id}>
                {user.isAdmin() ? <>
                    <td>{cpu.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(cpu)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(CPU_ROUTE + '/' + cpu.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteCpu(cpu.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === cpu.manufacturerId).name}</td>
                <td>{cpu.name}</td>
                <td>{parts.sockets_.find(socket => socket.id === cpu.socketId).name}</td>
                <td>{cpu.core}</td>
                <td>{cpu.thread}</td>
                <td>{cpu.clock} Mhz</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorCpu/>
    </div>);
});

export default CpuList;