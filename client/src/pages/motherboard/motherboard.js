import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchBus} from "../../http/busAPI";
import {fetchFormFactor} from "../../http/formFactorAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchMotherboard} from "../../http/motherboardAPI";
import {fetchRamType} from "../../http/ramTypeAPI";
import {fetchSocket} from "../../http/socketAPI";
import {fetchSsdInterface} from "../../http/ssdInterfaceAPI";

const Motherboard = () => {
    const [motherboard, setMotherboard] = useState({})
    const [bus, setBus] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const [socket, setSocket] = useState({})
    const [formFactor, setFormFactor] = useState({})
    const [ramType, setRamType] = useState({})
    const [ssdInterface, setSsdInterface] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchMotherboard(id).then(data => setMotherboard(data))
    }, [])

    useEffect(() => {
        fetchBus(parseInt(motherboard.busId) || 1).then(data => setBus(data))
        fetchManufacturer(parseInt(motherboard.manufacturerId) || 1).then(data => setManufacturer(data))
        fetchSocket(parseInt(motherboard.socketId) || 1).then(data => setSocket(data))
        fetchRamType(parseInt(motherboard.ramTypeId) || 1).then(data => setRamType(data))
        fetchSsdInterface(parseInt(motherboard.ssdInterfaceId) || 1).then(data => setSsdInterface(data))
        fetchFormFactor(parseInt(motherboard.formFactorId) || 1).then(data => setFormFactor(data))
    }, [motherboard])

    return (<Container>
        <div className="display-3">{manufacturer.name} {motherboard.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Сокет:</div>
                <div>Форм-фактор:</div>
                <div>Чіпсет:</div>
                <div>Тип оперативної пам'яті:</div>
                <div>Кіл-сть слотів оперативної пам'яті:</div>
                <div>Максимальний об'єм оперативної пам'яті:</div>
                <div>SSD інтерфейс:</div>
                <div>Кіл-сть конекторів M.2:</div>
                <div>Розрядність PCI конектору:</div>
            </Col>
            <Col>
                <div>{motherboard.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{motherboard.name}</div>
                <div>{socket.name}</div>
                <div>{formFactor.name}</div>
                <div>{motherboard.chipset}</div>
                <div>{ramType.name}</div>
                <div>{motherboard.ramCount}</div>
                <div>{motherboard.ramMaxCapacity} Mb</div>
                <div>{ssdInterface.name}</div>
                <div>{motherboard.m2Count}</div>
                <div>{bus.name}</div>
            </Col>
        </Row>
    </Container>);
};

export default Motherboard;