import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateMotherboard from "../../components/modals/createMotherboard";
import PaginatorMotherboard from "../../components/paginators/paginatorMotherboard";
import {fetchBuses} from "../../http/busAPI";
import {fetchFormFactors} from "../../http/formFactorAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {deleteMotherboard, fetchMotherboards} from "../../http/motherboardAPI";
import {fetchRamTypes} from "../../http/ramTypeAPI";
import {fetchSockets} from "../../http/socketAPI";
import {fetchSsdInterfaces} from "../../http/ssdInterfaceAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT, HOME_ROUTE, MOTHERBOARD_ROUTE} from "../../utils/consts";

const MotherboardList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [motherboardVisible, setMotherboardVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchSockets().then(data => parts.sockets_ = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchFormFactors().then(data => parts.formFactors = data)
        fetchRamTypes().then(data => parts.ramTypes = data)
        fetchSsdInterfaces().then(data => parts.ssdInterfaces = data)
        fetchBuses().then(data => parts.buses = data)
        fetchMotherboards(null, null, null, null, null, null, null, 1, limit).then(data => {
            parts.motherboards = data.rows
            pagination.motherboardTotalCount = data.count
            pagination.motherboardLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchMotherboards(searchText, filtering.motherboardSelectedFormFactor.id, filtering.motherboardSelectedManufacturer.id, filtering.motherboardSelectedSocket.id, filtering.motherboardSelectedRamType.id, filtering.motherboardSelectedSsdInterface.id, filtering.motherboardSelectedBus.id, pagination.motherboardActivePage, limit).then(data => {
            parts.motherboards = data.rows
            pagination.motherboardTotalCount = data.count
            pagination.motherboardLimit = limit
            if (pagination.motherboardTotalCount / pagination.motherboardLimit > pagination.motherboardActivePage) {
                pagination.motherboardActivePag = pagination.motherboardTotalCount / pagination.motherboardLimit
            }
        })
    }, [pagination.motherboardActivePage, filtering.motherboardSelectedFormFactor.id, filtering.motherboardSelectedManufacturer.id, filtering.motherboardSelectedSocket.id, filtering.motherboardSelectedRamType.id, filtering.motherboardSelectedSsdInterface.id, filtering.motherboardSelectedBus.id, searchText])

    function handleSelect(motherboard) {
        assembly.motherboard = motherboard
        assembly.ramCount = motherboard.ramCount
        filtering.gpuSelectedBus = parts.buses.find(bus => bus.id === motherboard.busId)
        filtering.gpuSelectedBusDisabled = true
        filtering.ramSelectedRamType = parts.ramTypes.find(ramType => ramType.id === motherboard.ramTypeId)
        filtering.ramSelectedRamTypeDisabled = true
        filtering.cpuSelectedSocket = parts.sockets_.find(socket => socket.id === motherboard.socketId)
        filtering.cpuSelectedSocketDisabled = true
        filtering.cpuCoolerSelectedSocket = parts.sockets_.find(socket => socket.id === motherboard.socketId)
        filtering.cpuCoolerSelectedSocketDisabled = true
        filtering.caseSelectedFormFactor = parts.formFactors.find(formFactor => formFactor.id === motherboard.formFactorId)
        filtering.caseSelectedFormFactorDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Материнські плати</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setMotherboardVisible(true)}
            >
                Додати материнську плату
            </Button>
            <CreateMotherboard show={motherboardVisible} onHide={() => setMotherboardVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            {filtering.motherboardSelectedFormFactorDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedFormFactor.name || 'Оберіть форм-фактор'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.formFactors.map(formFactor => <Dropdown.Item
                        key={formFactor.id}
                        onClick={() => filtering.motherboardSelectedFormFactor = formFactor}
                    >{formFactor.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedFormFactor = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            {filtering.motherboardSelectedSocketDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.sockets_.map(socket => <Dropdown.Item
                        key={socket.id}
                        onClick={() => filtering.motherboardSelectedSocket = socket}
                    >{socket.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedSocket = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.motherboardSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <div className="d-flex flex-row-reverse">
            {filtering.motherboardSelectedBusDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedBus.name || 'Оберіть розрядність PCI конектору'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.buses.map(bus => <Dropdown.Item
                        key={bus.id}
                        onClick={() => filtering.motherboardSelectedBus = bus}
                    >{bus.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedBus = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            {filtering.motherboardSelectedSsdInterfaceDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedSsdInterface.name || 'Оберіть інтерфейс підключення SSD'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.ssdInterfaces.map(ssdInterface => <Dropdown.Item
                        key={ssdInterface.id}
                        onClick={() => filtering.motherboardSelectedSsdInterface = ssdInterface}
                    >{ssdInterface.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedSsdInterface = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            {filtering.motherboardSelectedRamTypeDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.motherboardSelectedRamType.name || 'Оберіть тип оперативної пам\'яті'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.ramTypes.map(ramType => <Dropdown.Item
                        key={ramType.id}
                        onClick={() => filtering.motherboardSelectedRamType = ramType}
                    >{ramType.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.motherboardSelectedRamType = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
        </div>
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Сокет</th>
                <th>Форм-фактор</th>
                <th>Тип оперативної пам'яті</th>
            </tr>
            </thead>
            <tbody>
            {parts.motherboards.map(motherboard => <tr key={motherboard.id}>
                {user.isAdmin() ? <>
                    <td>{motherboard.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(motherboard)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(MOTHERBOARD_ROUTE + '/' + motherboard.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteMotherboard(motherboard.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === motherboard.manufacturerId).name}</td>
                <td>{motherboard.name}</td>
                <td>{parts.sockets_.find(socket => socket.id === motherboard.socketId).name}</td>
                <td>{parts.formFactors.find(formFactor => formFactor.id === motherboard.formFactorId).name}</td>
                <td>{parts.ramTypes.find(ramType => ramType.id === motherboard.ramTypeId).name}</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorMotherboard/>
    </div>);
});

export default MotherboardList;