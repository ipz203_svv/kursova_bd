import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchRam} from "../../http/ramAPI";
import {fetchRamType} from "../../http/ramTypeAPI";

const Ram = () => {
    const [ram, setRam] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const [ramType, setRamType] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchRam(id).then(data => setRam(data))
    }, [])


    useEffect(() => {
        fetchManufacturer(parseInt(ram.manufacturerId) || 1).then(data => setManufacturer(data))
        fetchRamType(parseInt(ram.ramTypeId) || 1).then(data => setRamType(data))
    }, [ram])

    return (<Container>
        <div className="display-3">{manufacturer.name} {ram.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Тип пам'яті:</div>
                <div>Об'єм:</div>
                <div>Частота:</div>
                <div>Має низький профіль?:</div>
            </Col>
            <Col>
                <div>{ram.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{ram.name}</div>
                <div>{ramType.name}</div>
                <div>{ram.size} Mb</div>
                <div>{ram.clock} MHz</div>
                <div>{ram.lowProfile ? 'Так' : 'Ні'}</div>
            </Col>
        </Row>
    </Container>);
};

export default Ram;