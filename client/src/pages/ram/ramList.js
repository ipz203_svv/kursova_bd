import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateRam from "../../components/modals/createRam";
import PaginatorRam from "../../components/paginators/paginatorRam";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {deleteRam, fetchRams} from "../../http/ramAPI";
import {fetchRamTypes} from "../../http/ramTypeAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT, HOME_ROUTE, RAM_ROUTE} from "../../utils/consts";

const RamList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [ramVisible, setRamVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchRamTypes().then(data => parts.ramTypes = data)
        fetchRams(null, null, null, 1, limit).then(data => {
            parts.rams = data.rows
            pagination.ramTotalCount = data.count
            pagination.ramLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchRams(searchText, filtering.ramSelectedManufacturer.id, filtering.ramSelectedRamType.id, pagination.ramActivePage, limit).then(data => {
            parts.rams = data.rows
            pagination.ramTotalCount = data.count
            pagination.ramLimit = limit
            if (pagination.ramTotalCount / pagination.ramLimit > pagination.ramActivePage) {
                pagination.ramActivePag = pagination.ramTotalCount / pagination.ramLimit
            }
        })
    }, [pagination.ramActivePage, filtering.ramSelectedManufacturer, filtering.ramSelectedRamType, searchText])

    function handleSelect(ram) {
        assembly.ram = ram
        filtering.motherboardSelectedRamType = parts.ramTypes.find(ramType => ramType.id === ram.ramTypeId)
        filtering.motherboardSelectedRamTypeDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Оперативна пам'ять</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setRamVisible(true)}
            >
                Додати оперативну пам'ять
            </Button>
            <CreateRam show={ramVisible} onHide={() => setRamVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.ramSelectedRamTypeDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.ramSelectedRamType.name || 'Оберіть тип пам\'яті'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.ramTypes.map(ramType => <Dropdown.Item
                        key={ramType.id}
                        onClick={() => filtering.ramSelectedRamType = ramType}
                    >{ramType.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.ramSelectedRamType = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.ramSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.ramSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.ramSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Розмір</th>
                <th>Тип пам'яті</th>
                <th>Частота</th>
            </tr>
            </thead>
            <tbody>
            {parts.rams.map(ram => <tr key={ram.id}>
                {user.isAdmin() ? <>
                    <td>{ram.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(ram)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(RAM_ROUTE + '/' + ram.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteRam(ram.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === ram.manufacturerId).name}</td>
                <td>{ram.name}</td>
                <td>{ram.size} MB</td>
                <td>{parts.ramTypes.find(ramType => ramType.id === ram.ramTypeId).name}</td>
                <td>{ram.clock} Mhz</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorRam/>
    </div>);
});

export default RamList;