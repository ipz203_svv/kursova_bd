import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Container, Nav} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import AssemblyComponent from "../components/assemblyComponent";
import PaginatorAssembly from "../components/paginators/paginatorAssembly";
import {fetchAssemblies} from "../http/assemblyAPI";
import {fetchManufacturers} from "../http/manufacturerAPI";
import {fetchRamTypes} from "../http/ramTypeAPI";
import {Context} from "../index";
import {GLOBAL_LIMIT} from "../utils/consts";

const Profile = observer(() => {
    const navigate = useNavigate();

    const {parts, user, pagination, filtering} = useContext(Context)
    const [assembliesLoading, setAssembliesLoading] = useState(true);
    const limit = GLOBAL_LIMIT

    useEffect(() => {
        filtering.assembliesSortMethod = 'my'
        fetchAssemblies(user.user.id, filtering.assembliesSortMethod, 1, 5).then(data => {
            parts.assemblies = data.rows
            fetchManufacturers().then(data => parts.manufacturers = data)
            fetchRamTypes().then(data => parts.ramTypes = data)
            setAssembliesLoading(false)
        })
    }, [])

    useEffect(() => {
        fetchAssemblies(user.user.id, filtering.assembliesSortMethod, pagination.assemblyActivePage, limit).then(data => {
            parts.assemblies = data.rows
            pagination.assemblyTotalCount = data.count
            pagination.assemblyLimit = limit
            if (pagination.assemblyTotalCount / pagination.assemblyLimit > pagination.assemblyActivePage) {
                pagination.assemblyActivePag = pagination.assemblyTotalCount / pagination.assemblyLimit
            }
            setAssembliesLoading(false)
        })
    }, [pagination.assemblyActivePage, filtering.assembliesSortMethod])

    if (assembliesLoading) {
        return <div>Loading...</div>
    }

    return (<Container style={{marginTop: 30 + 'px'}}>
        <Nav variant="tabs" defaultActiveKey="my">
            <Nav.Item>
                <Nav.Link eventKey="my" onClick={() => filtering.assembliesSortMethod = 'my'}>Ваші збірки</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="liked" onClick={() => filtering.assembliesSortMethod = 'liked'}>Збірки, що ви
                    вподобали</Nav.Link>
            </Nav.Item>
        </Nav>
        {parts.assemblies.map(assembly => <AssemblyComponent key={assembly.id} assembly={assembly}/>)}
        <PaginatorAssembly/>
    </Container>);
});

export default Profile;