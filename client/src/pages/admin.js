import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Container, Table} from "react-bootstrap";
import CreateBus from "../components/modals/createBus";
import CreateFanSize from "../components/modals/createFanSize";
import CreateFormFactor from "../components/modals/createFormFactor";
import CreateManufacturer from "../components/modals/createManufacturer";
import CreateRamType from "../components/modals/createRamType";
import CreateSocket from "../components/modals/createSocket";
import CreateSsdInterface from "../components/modals/createSsdInterface";
import AssembliesStats from "../components/stats/assembliesStats";
import UsersStats from "../components/stats/usersStats";
import {deleteBus, fetchBuses} from "../http/busAPI";
import {deleteFanSize, fetchFanSizes} from "../http/fanSizeAPI";
import {deleteFormFactor, fetchFormFactors} from "../http/formFactorAPI";
import {deleteManufacturer, fetchManufacturers} from "../http/manufacturerAPI";
import {deleteRamType, fetchRamTypes} from "../http/ramTypeAPI";
import {deleteSocket, fetchSockets} from "../http/socketAPI";
import {deleteSsdInterface, fetchSsdInterfaces} from "../http/ssdInterfaceAPI";
import {Context} from "../index";

const Admin = observer(() => {
    const {parts, user} = useContext(Context)
    const [manufacturerVisible, setManufacturerVisible] = useState(false)
    const [socketVisible, setSocketVisible] = useState(false)
    const [busVisible, setBusVisible] = useState(false)
    const [formFactorVisible, setFormFactorVisible] = useState(false)
    const [ramTypeVisible, setRamTypeVisible] = useState(false)
    const [ssdInterfaceVisible, setSsdInterfaceVisible] = useState(false)
    const [fanSizeVisible, setFanSizeVisible] = useState(false)

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchSockets().then(data => parts.sockets_ = data)
        fetchBuses().then(data => parts.buses = data)
        fetchFormFactors().then(data => parts.formFactors = data)
        fetchRamTypes().then(data => parts.ramTypes = data)
        fetchSsdInterfaces().then(data => parts.ssdInterfaces = data)
        fetchFanSizes().then(data => parts.fanSizes = data)
    }, [parts])

    return (<Container className="d-flex flex-column">
        <div className="d-flex flex-column">
            <div className="display-3">Виробники</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.manufacturers.map(manufacturer => <tr key={manufacturer.id}>
                    <td>{manufacturer.id}</td>
                    <td>{manufacturer.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteManufacturer(manufacturer.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setManufacturerVisible(true)}
            >
                Додати Виробника
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">Сокети</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.sockets_.map(socket => <tr key={socket.id}>
                    <td>{socket.id}</td>
                    <td>{socket.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteSocket(socket.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setSocketVisible(true)}
            >
                Додати Сокет
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">Розрядності PCI конекторів</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.buses.map(bus => <tr key={bus.id}>
                    <td>{bus.id}</td>
                    <td>{bus.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteBus(bus.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setBusVisible(true)}
            >
                Додати Розрядність
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">Форм-фактори</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.formFactors.map(formFactor => <tr key={formFactor.id}>
                    <td>{formFactor.id}</td>
                    <td>{formFactor.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteFormFactor(formFactor.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setFormFactorVisible(true)}
            >
                Додати Форм-фактор
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">Типи пам'яті</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.ramTypes.map(ramType => <tr key={ramType.id}>
                    <td>{ramType.id}</td>
                    <td>{ramType.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteRamType(ramType.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setRamTypeVisible(true)}
            >
                Додати Тип пам'яті
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">SSD інтерфейси</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.ssdInterfaces.map(ssdInterface => <tr key={ssdInterface.id}>
                    <td>{ssdInterface.id}</td>
                    <td>{ssdInterface.name}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteSsdInterface(ssdInterface.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setSsdInterfaceVisible(true)}
            >
                Додати SSD інтерфейс
            </Button>
        </div>

        <div className="d-flex flex-column">
            <div className="display-3">Розміри вентилятору</div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Назва</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                {parts.fanSizes.map(fanSize => <tr key={fanSize.id}>
                    <td>{fanSize.id}</td>
                    <td>{fanSize.size}</td>
                    <td>{user.isAdmin() ? <Button className="w-25" variant="warning"
                                                  onClick={() => deleteFanSize(fanSize.id)}>Видалити</Button> : ''}</td>
                </tr>)}
                </tbody>
            </Table>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setFanSizeVisible(true)}
            >
                Додати розмір вентилятору
            </Button>
        </div>
        <UsersStats/>
        <AssembliesStats/>
        <CreateManufacturer show={manufacturerVisible} onHide={() => setManufacturerVisible(false)}/>
        <CreateSocket show={socketVisible} onHide={() => setSocketVisible(false)}/>
        <CreateBus show={busVisible} onHide={() => setBusVisible(false)}/>
        <CreateFormFactor show={formFactorVisible} onHide={() => setFormFactorVisible(false)}/>
        <CreateRamType show={ramTypeVisible} onHide={() => setRamTypeVisible(false)}/>
        <CreateSsdInterface show={ssdInterfaceVisible} onHide={() => setSsdInterfaceVisible(false)}/>
        <CreateFanSize show={fanSizeVisible} onHide={() => setFanSizeVisible(false)}/>
    </Container>);
})

export default Admin;