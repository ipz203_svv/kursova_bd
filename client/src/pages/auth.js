import {observer} from "mobx-react-lite";
import React, {useContext, useState} from 'react';
import {Button, Card, Col, Container, Form, Row} from "react-bootstrap";
import {NavLink, useLocation, useNavigate} from "react-router-dom";
import {login, registration} from "../http/userAPI";
import {Context} from "../index";
import {HOME_ROUTE, LOGIN_ROUTE, REGISTRATION_ROUTE} from "../utils/consts";

const Auth = observer(() => {
    const route = useLocation()
    const navigate = useNavigate()
    const isLogin = route.pathname === LOGIN_ROUTE

    const {user} = useContext(Context)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')

    const authClick = async () => {
        try {
            let data
            if (isLogin) {
                data = await login(email, password)
            } else {
                data = await registration(email, password)
            }

            user.setUser(data)
            user.setIsAuth(true)

            navigate(HOME_ROUTE)
        } catch (e) {
            setError(e.response.data.message)
        }
    }

    return (
        <Container className="d-flex justify-content-center align-items-center mt-5">
            <Card className="p-5 w-auto">
                <p className="display-2 m-auto">
                    {isLogin ? 'Вхід' : 'Реєстрація'}
                </p>
                {error !== '' ?
                    <p className="h6 text-bg-danger text-white m-auto">
                        {error}
                    </p>
                    : ''
                }
                <Form className="d-flex flex-column mt-3">
                    <Form.Control className="mt-2" placeholder="Ваш e-mail..." autoComplete="username"
                                  value={email}
                                  onChange={e => setEmail(e.target.value)}/>
                    <Form.Control className="mt-2" placeholder="Ваш пароль..." type="password"
                                  autoComplete="current-password"
                                  value={password}
                                  onChange={e => setPassword(e.target.value)}/>
                    <Row className="d-flex mt-4 ali">
                        <Col className="col-12 col-lg-6">
                            {isLogin ?
                                <Card.Text>Не маєте аккаунту? <NavLink className="link-dark"
                                                                       to={REGISTRATION_ROUTE}>Зареєструйтесь!</NavLink></Card.Text>
                                :
                                <Card.Text>Вже є аккаунт? <NavLink className="link-dark"
                                                                   to={LOGIN_ROUTE}>Увійдіть!</NavLink></Card.Text>
                            }
                        </Col>
                        <Col className="col-12 col-lg-6">
                            <Button variant={"dark"}
                                    onClick={authClick}
                                    className="w-100">{isLogin ? 'Увійти' : 'Зареєструватись'}</Button>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </Container>
    );
});

export default Auth;