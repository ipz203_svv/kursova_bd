import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateCaseFan from "../../components/modals/createCaseFan";
import PaginatorCaseFan from "../../components/paginators/paginatorCaseFan";
import {deleteCaseFan, fetchCaseFans} from "../../http/caseFanAPI";
import {fetchFanSizes} from "../../http/fanSizeAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";
import {CASE_FAN_ROUTE, GLOBAL_LIMIT, HOME_ROUTE} from "../../utils/consts";

const CaseFanList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [caseFanVisible, setCaseFanVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchFanSizes().then(data => parts.fanSizes = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchCaseFans(null, null, null, 1, limit).then(data => {
            parts.caseFans = data.rows
            pagination.caseFanTotalCount = data.count
            pagination.caseFanLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchCaseFans(searchText, filtering.caseFanSelectedManufacturer.id, filtering.caseFanSelectedFanSize.id, pagination.caseFanActivePage, limit).then(data => {
            parts.caseFans = data.rows
            pagination.caseFanTotalCount = data.count
            pagination.caseFanLimit = limit
            if (pagination.caseFanTotalCount / pagination.caseFanLimit > pagination.caseFanActivePage) {
                pagination.caseFanActivePag = pagination.caseFanTotalCount / pagination.caseFanLimit
            }
        })
    }, [pagination.caseFanActivePage, filtering.caseFanSelectedManufacturer, filtering.caseFanSelectedFanSize, searchText])

    function handleSelect(caseFan) {
        assembly.caseFan = caseFan
        filtering.caseSelectedFanSize = parts.fanSizes.find(fanSize => fanSize.id === caseFan.fanSizeId)
        filtering.caseSelectedFanSizeDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Вентилятор корпусу</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setCaseFanVisible(true)}
            >
                Додати вентилятор корпусу
            </Button>
            <CreateCaseFan show={caseFanVisible} onHide={() => setCaseFanVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.caseFanSelectedFanSizeDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.caseFanSelectedFanSize.size || 'Оберіть розмір вентилятору'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.fanSizes.map(fanSize => <Dropdown.Item
                        key={fanSize.id}
                        onClick={() => filtering.caseFanSelectedFanSize = fanSize}
                    >{fanSize.size}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.caseFanSelectedFanSize = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.caseFanSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.caseFanSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.caseFanSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Розмір</th>
                <th>Швидкість обертання</th>
            </tr>
            </thead>
            <tbody>
            {parts.caseFans.map(caseFan => <tr key={caseFan.id}>
                {user.isAdmin() ? <>
                    <td>{caseFan.id}</td>
                </> : ""}
                <td>
                    <Button onClick={() => {
                        handleSelect(caseFan)
                    }}>Обрати</Button>
                    <Button onClick={() => navigate(CASE_FAN_ROUTE + '/' + caseFan.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteCaseFan(caseFan.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === caseFan.manufacturerId).name}</td>
                <td>{caseFan.name}</td>
                <td>{parts.fanSizes.find(fanSize => fanSize.id === caseFan.fanSizeId).size} mm</td>
                <td>{caseFan.rpm} RPM</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorCaseFan/>
    </div>);
});

export default CaseFanList;