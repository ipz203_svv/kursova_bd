import React, {useEffect, useLayoutEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchCaseFan} from "../../http/caseFanAPI";
import {fetchFanSize} from "../../http/fanSizeAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";

const CaseFan = () => {
    const [caseFan, setCaseFan] = useState({})
    const [fanSize, setFanSize] = useState({size: 'fanSize'})
    const [manufacturer, setManufacturer] = useState({name: 'manufacturer'})
    const {id} = useParams()

    useEffect(() => {
        fetchCaseFan(id).then(data => {
            setCaseFan(data)
        })
    }, [])

    useLayoutEffect(() => {
        fetchFanSize(parseInt(caseFan.fanSizeId) || 2).then(data => setFanSize(data))
        fetchManufacturer(parseInt(caseFan.manufacturerId) || 1).then(data => setManufacturer(data))
    }, [caseFan])

    return (<Container>
        <div className="display-3">{manufacturer.name} {caseFan.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Розмір:</div>
                <div>Швидкість обертання:</div>
            </Col>
            <Col>
                <div>{caseFan.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{caseFan.name}</div>
                <div>{fanSize.size} mm</div>
                <div>{caseFan.rpm} RPM</div>
            </Col>
        </Row>
    </Container>);
};

export default CaseFan;