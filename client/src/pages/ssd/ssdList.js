import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateSsd from "../../components/modals/createSsd";
import PaginatorSsd from "../../components/paginators/paginatorSsd";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {deleteSsd, fetchSsds} from "../../http/ssdAPI";
import {fetchSsdInterfaces} from "../../http/ssdInterfaceAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT, HOME_ROUTE, SSD_ROUTE} from "../../utils/consts";

const SsdList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [ssdVisible, setSsdVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchSsdInterfaces().then(data => parts.ssdInterfaces = data)
        fetchSsds(null, null, null, 1, limit).then(data => {
            parts.ssds = data.rows
            pagination.ssdTotalCount = data.count
            pagination.ssdLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchSsds(searchText, filtering.ssdSelectedManufacturer.id, filtering.ssdSelectedSsdInterface.id, pagination.ssdActivePage, limit).then(data => {
            parts.ssds = data.rows
            pagination.ssdTotalCount = data.count
            pagination.ssdLimit = limit
            if (pagination.ssdTotalCount / pagination.ssdLimit > pagination.ssdActivePage) {
                pagination.ssdActivePag = pagination.ssdTotalCount / pagination.ssdLimit
            }
        })
    }, [pagination.ssdActivePage, filtering.ssdSelectedManufacturer, filtering.ssdSelectedSsdInterface, searchText])

    function handleSelect(ssd) {
        assembly.ssd = ssd
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Накопичувачі SSD</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setSsdVisible(true)}
            >
                Додати накопичувач
            </Button>
            <CreateSsd show={ssdVisible} onHide={() => setSsdVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.ssdSelectedSsdInterface.name || 'Оберіть інтерфейс'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.ssdInterfaces.map(ssdInterface => <Dropdown.Item
                        key={ssdInterface.id}
                        onClick={() => filtering.ssdSelectedSsdInterface = ssdInterface}
                    >{ssdInterface.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.ssdSelectedSsdInterface = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.ssdSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.ssdSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.ssdSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Об'єм</th>
                <th>Швидкість запису</th>
                <th>Швидкість читання</th>
                <th>Інтерфейс</th>
            </tr>
            </thead>
            <tbody>
            {parts.ssds.map(ssd => <tr key={ssd.id}>
                {user.isAdmin() ? <>
                    <td>{ssd.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(ssd)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(SSD_ROUTE + '/' + ssd.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteSsd(ssd.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === ssd.manufacturerId).name}</td>
                <td>{ssd.name}</td>
                <td>{ssd.size} GB</td>
                <td>{ssd.writeSpeed} MB/s</td>
                <td>{ssd.readSpeed} MB/s</td>
                <td>{parts.ssdInterfaces.find(ssdInterface => ssdInterface.id === ssd.ssdInterfaceId).name}</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorSsd/>
    </div>);
});

export default SsdList;