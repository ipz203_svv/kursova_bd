import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchSsd} from "../../http/ssdAPI";
import {fetchSsdInterface} from "../../http/ssdInterfaceAPI";

const Ssd = () => {
    const [ssd, setSsd] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const [ssdInterface, setSsdInterface] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchSsd(id).then(data => setSsd(data))
    }, [])

    useEffect(() => {
        fetchManufacturer(parseInt(ssd.manufacturerId) || 1).then(data => setManufacturer(data))
        fetchSsdInterface(parseInt(ssd.ssdInterfaceId) || 1).then(data => setSsdInterface(data))
    }, [ssd])

    return (<Container>
        <div className="display-3">{manufacturer.name} {ssd.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Об'єм:</div>
                <div>Швидкість запису:</div>
                <div>Швидкість читання:</div>
                <div>SSD інтерфейс:</div>
            </Col>
            <Col>
                <div>{ssd.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{ssd.name}</div>
                <div>{ssd.size} Gb</div>
                <div>{ssd.readSpeed} Mb/s</div>
                <div>{ssd.writeSpeed} Mb/s</div>
                <div>{ssdInterface.name}</div>
            </Col>
        </Row>
    </Container>);
};

export default Ssd;