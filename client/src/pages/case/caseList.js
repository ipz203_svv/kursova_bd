import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateCase from "../../components/modals/createCase";
import PaginatorCase from "../../components/paginators/paginatorCase";
import {deleteCase, fetchCases} from "../../http/caseAPI";
import {fetchFanSizes} from "../../http/fanSizeAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";
import {CASE_ROUTE, GLOBAL_LIMIT, HOME_ROUTE} from "../../utils/consts";

const CaseList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [caseVisible, setCaseVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchFanSizes().then(data => parts.fanSizes = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchCases(null, null, null, 1, limit).then(data => {
            parts.cases = data.rows
            pagination.caseTotalCount = data.count
            pagination.caseLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchCases(searchText, filtering.caseSelectedManufacturer.id, filtering.caseSelectedFormFactor.id, filtering.caseSelectedFanSize.id, pagination.caseActivePage, limit).then(data => {
            parts.cases = data.rows
            pagination.caseTotalCount = data.count
            pagination.caseLimit = limit
            if (pagination.caseTotalCount / pagination.caseLimit > pagination.caseActivePage) {
                pagination.caseActivePag = pagination.caseTotalCount / pagination.caseLimit
            }
        })
    }, [pagination.caseActivePage, filtering.caseSelectedManufacturer, filtering.caseSelectedFanSize, searchText])

    function handleSelect(case_) {
        assembly.case_ = case_
        assembly.caseFanCount = case_.fanCount
        filtering.caseFanSelectedFanSize = parts.fanSizes.find(fanSize => fanSize.id === case_.fanSizeId)
        filtering.caseFanSelectedFanSizeDisabled = true
        filtering.motherboardSelectedFormFactor = parts.formFactors.find(formFactor => formFactor.id === case_.formFactorId)
        filtering.motherboardSelectedFormFactorDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Корпус</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setCaseVisible(true)}
            >
                Додати корпус
            </Button>
            <CreateCase show={caseVisible} onHide={() => setCaseVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.caseSelectedFanSizeDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.caseSelectedFanSize.size || 'Оберіть розмір вентилятору'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.fanSizes.map(fanSize => <Dropdown.Item
                        key={fanSize.id}
                        onClick={() => filtering.caseSelectedFanSize = fanSize}
                    >{fanSize.size}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.caseSelectedFanSize = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.caseSelectedFormFactor.name || 'Оберіть форм-фактор'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.formFactors.map(formFactor => <Dropdown.Item
                        key={formFactor.id}
                        onClick={() => filtering.caseSelectedFormFactor = formFactor}
                    >{formFactor.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.caseSelectedFormFactor = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.caseSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.caseSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.caseSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Форм-фактор</th>
                <th>Розмір вентиляторів</th>
                <th>Кіл-сть вентиляторів</th>
                <th>Ширина</th>
                <th>Висота</th>
            </tr>
            </thead>
            <tbody>
            {parts.cases.map(case_ => <tr key={case_.id}>
                {user.isAdmin() ? <>
                    <td>{case_.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(case_)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(CASE_ROUTE + '/' + case_.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteCase(case_.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === case_.manufacturerId).name}</td>
                <td>{case_.name}</td>
                <td>{parts.formFactors.find(formFactor => formFactor.id === case_.formFactorId).name}</td>
                <td>{parts.fanSizes.find(fanSize => fanSize.id === case_.fanSizeId).size} mm</td>
                <td>{case_.fanCount}</td>
                <td>{case_.width} mm</td>
                <td>{case_.height} mm</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorCase/>
    </div>);
});

export default CaseList;