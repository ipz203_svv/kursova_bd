import React, {useEffect, useLayoutEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchCase} from "../../http/caseAPI";
import {fetchFanSize} from "../../http/fanSizeAPI";
import {fetchFormFactor} from "../../http/formFactorAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";

const Case = () => {
    const [case_, setCase] = useState({})
    const [fanSize, setFanSize] = useState({size: 'fanSize'})
    const [manufacturer, setManufacturer] = useState({name: 'manufacturer'})
    const [formFactor, setFormFactor] = useState({name: 'formFactor'})
    const {id} = useParams()

    useEffect(() => {
        fetchCase(id).then(data => {
            setCase(data)
        })
    }, [])

    useLayoutEffect(() => {
        fetchFanSize(parseInt(case_.fanSizeId) || 2).then(data => setFanSize(data))
        fetchManufacturer(parseInt(case_.manufacturerId) || 1).then(data => setManufacturer(data))
        fetchFormFactor(parseInt(case_.formFactorId) || 1).then(data => setFormFactor(data))
    }, [case_])

    return (<Container>
        <div className="display-3">{manufacturer.name} {case_.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Форм-фактор:</div>
                <div>Ширина:</div>
                <div>Висота:</div>
                <div>Внутрішня довжина:</div>
                <div>Глубина:</div>
                <div>Максимальна довжина блоку живлення:</div>
                <div>Кіл-сть кріплень для вентилятору:</div>
                <div>Розмір вентиляторів:</div>
            </Col>
            <Col>
                <div>{case_.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{case_.name}</div>
                <div>{formFactor.name}</div>
                <div>{case_.width} mm</div>
                <div>{case_.height} mm</div>
                <div>{case_.innerLength} mm</div>
                <div>{case_.depth} mm</div>
                <div>{case_.maxPsuLength} mm</div>
                <div>{case_.fanCount}</div>
                <div>{fanSize.size} mm</div>
            </Col>
        </Row>
    </Container>);
};

export default Case;