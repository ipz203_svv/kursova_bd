import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchManufacturer} from "../../http/manufacturerAPI";
import {fetchPowerSupply} from "../../http/powerSupplyAPI";

const PowerSupply = () => {
    const [powerSupply, setPowerSupply] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchPowerSupply(id).then(data => setPowerSupply(data))
    }, [])

    useEffect(() => {
        fetchManufacturer(parseInt(powerSupply.manufacturerId) || 1).then(data => setManufacturer(data))
    }, [powerSupply])

    return (<Container>
        <div className="display-3">{manufacturer.name} {powerSupply.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Максимальна потужність:</div>
                <div>Рейтинг 80plus:</div>
                <div>Довжина:</div>
            </Col>
            <Col>
                <div>{powerSupply.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{powerSupply.name}</div>
                <div>{powerSupply.maxPower} W</div>
                <div>{powerSupply.rating80Plus}</div>
                <div>{powerSupply.psuLength} mm</div>
            </Col>
        </Row>
    </Container>);
};

export default PowerSupply;