import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreatePowerSupply from "../../components/modals/createPowerSupply";
import PaginatorPowerSupply from "../../components/paginators/paginatorPowerSupply";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {deletePowerSupply, fetchPowerSupplies} from "../../http/powerSupplyAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT, HOME_ROUTE, POWER_SUPPLY_ROUTE} from "../../utils/consts";

const PowerSupplyList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [powerSupplyVisible, setPowerSupplyVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchPowerSupplies(null, null, 1, limit).then(data => {
            parts.powerSupplies = data.rows
            pagination.powerSupplyTotalCount = data.count
            pagination.powerSupplyLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchPowerSupplies(searchText, filtering.powerSupplySelectedManufacturer.id, pagination.powerSupplyActivePage, limit).then(data => {
            parts.powerSupplies = data.rows
            pagination.powerSupplyTotalCount = data.count
            pagination.powerSupplyLimit = limit
            if (pagination.powerSupplyTotalCount / pagination.powerSupplyLimit > pagination.powerSupplyActivePage) {
                pagination.powerSupplyActivePag = pagination.powerSupplyTotalCount / pagination.powerSupplyLimit
            }
        })
    }, [pagination.powerSupplyActivePage, filtering.powerSupplySelectedManufacturer, searchText])

    function handleSelect(powerSupply) {
        assembly.powerSupply = powerSupply
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Блок живлення</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setPowerSupplyVisible(true)}
            >
                Додати блок живлення
            </Button>
            <CreatePowerSupply show={powerSupplyVisible} onHide={() => setPowerSupplyVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.powerSupplySelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.powerSupplySelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.powerSupplySelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Максимальне навантаження</th>
                <th>Рейтинг 80plus</th>
            </tr>
            </thead>
            <tbody>
            {parts.powerSupplies.map(powerSupply => <tr key={powerSupply.id}>
                {user.isAdmin() ? <>
                    <td>{powerSupply.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(powerSupply)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(POWER_SUPPLY_ROUTE + '/' + powerSupply.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deletePowerSupply(powerSupply.id)}>Видалити</Button> : ''}
                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === powerSupply.manufacturerId).name}</td>
                <td>{powerSupply.name}</td>
                <td>{powerSupply.maxPower}W</td>
                <td>{powerSupply.rating80Plus}</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorPowerSupply/>
    </div>);
});

export default PowerSupplyList;