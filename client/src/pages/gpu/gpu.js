import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {fetchBus} from "../../http/busAPI";
import {fetchGpu} from "../../http/gpuAPI";
import {fetchManufacturer} from "../../http/manufacturerAPI";

const Gpu = () => {
    const [gpu, setGpu] = useState({})
    const [bus, setBus] = useState({})
    const [manufacturer, setManufacturer] = useState({})
    const {id} = useParams()

    useEffect(() => {
        fetchGpu(id).then(data => setGpu(data))
    }, [])

    useEffect(() => {
        fetchBus(parseInt(gpu.busId) || 1).then(data => setBus(data))
        fetchManufacturer(parseInt(gpu.manufacturerId) || 1).then(data => setManufacturer(data))
    }, [gpu])

    return (<Container>
        <div className="display-3">{manufacturer.name} {gpu.name}</div>
        <Row>
            <Col>
                <div>Рік випуску:</div>
                <div>Виробник:</div>
                <div>Назва:</div>
                <div>Об'єм відео-пам'яті:</div>
                <div>Відео-чіп:</div>
                <div>Частота відео-чіпу:</div>
                <div>Частота відео-пам'яті:</div>
                <div>Тип відео-пам'яті:</div>
                <div>Тепло, що виділяється:</div>
                <div>Довжина:</div>
                <div>Розрядність PCI конектору:</div>
            </Col>
            <Col>
                <div>{gpu.releaseYear}</div>
                <div>{manufacturer.name}</div>
                <div>{gpu.name}</div>
                <div>{gpu.memorySize} Mb</div>
                <div>{gpu.chip}</div>
                <div>{gpu.gpuClock} MHz</div>
                <div>{gpu.memoryClock} MHz</div>
                <div>{gpu.gmemoryType}</div>
                <div>{gpu.tdp} W</div>
                <div>{gpu.length} mm</div>
                <div>{bus.name}</div>
            </Col>
        </Row>
    </Container>);
};

export default Gpu;