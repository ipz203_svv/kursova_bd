import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, FormControl, InputGroup, Table} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import CreateGpu from "../../components/modals/createGpu";
import PaginatorGpu from "../../components/paginators/paginatorGpu";
import {fetchBuses} from "../../http/busAPI";
import {deleteGpu, fetchGpus} from "../../http/gpuAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";
import {GLOBAL_LIMIT, GPU_ROUTE, HOME_ROUTE} from "../../utils/consts";

const GpuList = observer(() => {
    const navigate = useNavigate();
    const {parts, user, pagination, filtering, assembly} = useContext(Context)

    const [gpuVisible, setGpuVisible] = useState(false)
    const [searchText, setSearchText] = useState('')

    const limit = GLOBAL_LIMIT

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchBuses().then(data => parts.buses = data)
        fetchGpus(null, null, null, 1, limit).then(data => {
            parts.gpus = data.rows
            pagination.gpuTotalCount = data.count
            pagination.gpuLimit = limit
        })
    }, [])

    useEffect(() => {
        fetchGpus(searchText, filtering.gpuSelectedManufacturer.id, filtering.gpuSelectedBus.id, pagination.gpuActivePage, limit).then(data => {
            parts.gpus = data.rows
            pagination.gpuTotalCount = data.count
            pagination.gpuLimit = limit
            if (pagination.gpuTotalCount / pagination.gpuLimit > pagination.gpuActivePage) {
                pagination.gpuActivePag = pagination.gpuTotalCount / pagination.gpuLimit
            }
        })
    }, [pagination.gpuActivePage, filtering.gpuSelectedManufacturer, filtering.gpuSelectedBus, searchText])

    function handleSelect(gpu) {
        assembly.gpu = gpu
        filtering.motherboardSelectedBus = parts.buses.find(bus => bus.id === gpu.busId)
        filtering.motherboardSelectedBusDisabled = true
        navigate(HOME_ROUTE)
    }

    return (<div className="container">
        <div className="display-3">Відеокарти</div>
        {user.isAdmin() ? <>
            <Button
                variant={"dark"}
                className="mt-4 p-2"
                onClick={() => setGpuVisible(true)}
            >
                Додати Відеокарту
            </Button>
            <CreateGpu show={gpuVisible} onHide={() => setGpuVisible(false)}/>
        </> : ''}
        <div className="d-flex flex-row-reverse">
            <InputGroup>
                <FormControl
                    placeholder="Пошук за назвою"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </InputGroup>
            {filtering.gpuSelectedBusDisabled ? '' : <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.gpuSelectedBus.name || 'Оберіть розрядність PCI'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.buses.map(bus => <Dropdown.Item
                        key={bus.id}
                        onClick={() => filtering.gpuSelectedBus = bus}
                    >{bus.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.gpuSelectedBus = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>}
            <Dropdown className="mt-2 mb-2">
                <Dropdown.Toggle>{filtering.gpuSelectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                <Dropdown.Menu>
                    {parts.manufacturers.map(manufacturer => <Dropdown.Item
                        key={manufacturer.id}
                        onClick={() => filtering.gpuSelectedManufacturer = manufacturer}
                    >{manufacturer.name}</Dropdown.Item>)}
                    <Dropdown.Item
                        key={0}
                        onClick={() => filtering.gpuSelectedManufacturer = {}}
                    >Відмінити фільтр</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
        <Table striped bordered hover>
            <thead>
            <tr>
                {user.isAdmin() ? <>
                    <th>id</th>
                </> : ""}
                <th>Дії</th>
                <th>Виробник</th>
                <th>Назва</th>
                <th>Кіл-сть пам'яті</th>
                <th>Тип пам'яті</th>
                <th>Частота чіпу</th>
                <th>Частота пам'яті</th>
            </tr>
            </thead>
            <tbody>
            {parts.gpus.map(gpu => <tr key={gpu.id}>
                {user.isAdmin() ? <>
                    <td>{gpu.id}</td>
                </> : ""}
                <td><Button onClick={() => {
                    handleSelect(gpu)
                }}>Обрати</Button>
                    <Button onClick={() => navigate(GPU_ROUTE + '/' + gpu.id)}
                            className="cursor-pointer">Детальніше</Button>
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteGpu(gpu.id)}>Видалити</Button> : ''}

                </td>
                <td>{parts.manufacturers.find(manufacturer => manufacturer.id === gpu.manufacturerId).name}</td>
                <td>{gpu.name}</td>
                <td>{gpu.memorySize}</td>
                <td>{gpu.gmemoryType}</td>
                <td>{gpu.gpuClock} Mhz</td>
                <td>{gpu.memoryClock} Mhz</td>
            </tr>)}
            </tbody>
        </Table>
        <PaginatorGpu/>
    </div>);
});

export default GpuList;