import Admin from "./pages/admin";
import Assembly from "./pages/assembly/assembly";
import AssemblyList from "./pages/assembly/assemblyList";
import Auth from "./pages/auth";
import Case from "./pages/case/case";
import CaseList from "./pages/case/caseList";
import CaseFan from "./pages/caseFan/caseFan";
import CaseFanList from "./pages/caseFan/caseFanList";
import Cpu from "./pages/cpu/cpu";
import CpuList from "./pages/cpu/cpuList";
import CpuCooler from "./pages/cpuCooler/cpuCooler";
import CpuCoolerList from "./pages/cpuCooler/cpuCoolerList";
import Gpu from "./pages/gpu/gpu";
import GpuList from "./pages/gpu/gpuList";
import Home from "./pages/home";
import Motherboard from "./pages/motherboard/motherboard";
import MotherboardList from "./pages/motherboard/motherboardList";
import PowerSupply from "./pages/powerSupply/powerSupply";
import PowerSupplyList from "./pages/powerSupply/powerSupplyList";
import Profile from "./pages/profile";
import Ram from "./pages/ram/ram";
import RamList from "./pages/ram/ramList";
import Ssd from "./pages/ssd/ssd";
import SsdList from "./pages/ssd/ssdList";
import {
    ADMIN_ROUTE,
    ASSEMBLY_ROUTE,
    CASE_FAN_ROUTE,
    CASE_ROUTE,
    CPU_COOLER_ROUTE,
    CPU_ROUTE,
    GPU_ROUTE,
    HOME_ROUTE,
    LOGIN_ROUTE,
    MOTHERBOARD_ROUTE,
    POWER_SUPPLY_ROUTE,
    PROFILE_ROUTE,
    RAM_ROUTE,
    REGISTRATION_ROUTE,
    SSD_ROUTE
} from './utils/consts'

export const publicRoutes = [{
    path: HOME_ROUTE, Component: Home
}, {
    path: REGISTRATION_ROUTE, Component: Auth
}, {
    path: LOGIN_ROUTE, Component: Auth
}, {
    path: CPU_ROUTE, Component: CpuList
}, {
    path: CPU_ROUTE + '/:id', Component: Cpu
}, {
    path: GPU_ROUTE, Component: GpuList
}, {
    path: GPU_ROUTE + '/:id', Component: Gpu
}, {
    path: CASE_FAN_ROUTE, Component: CaseFanList
}, {
    path: CASE_FAN_ROUTE + '/:id', Component: CaseFan
}, {
    path: CASE_ROUTE, Component: CaseList
}, {
    path: CASE_ROUTE + '/:id', Component: Case
}, {
    path: CPU_COOLER_ROUTE, Component: CpuCoolerList
}, {
    path: CPU_COOLER_ROUTE + '/:id', Component: CpuCooler
}, {
    path: MOTHERBOARD_ROUTE, Component: MotherboardList
}, {
    path: MOTHERBOARD_ROUTE + '/:id', Component: Motherboard
}, {
    path: POWER_SUPPLY_ROUTE, Component: PowerSupplyList
}, {
    path: POWER_SUPPLY_ROUTE + '/:id', Component: PowerSupply
}, {
    path: RAM_ROUTE, Component: RamList
}, {
    path: RAM_ROUTE + '/:id', Component: Ram
}, {
    path: SSD_ROUTE, Component: SsdList
}, {
    path: SSD_ROUTE + '/:id', Component: Ssd
}]

export const authRoutes = [{
    path: ASSEMBLY_ROUTE, Component: AssemblyList
}, {
    path: ASSEMBLY_ROUTE + '/:id', Component: Assembly
}, {
    path: PROFILE_ROUTE, Component: Profile
}, {
    path: ADMIN_ROUTE, Component: Admin
}]
