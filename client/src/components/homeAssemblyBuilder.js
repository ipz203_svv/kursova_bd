import {observer} from "mobx-react-lite";
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Button, Card, Col, Dropdown, Row} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import {createAssembly} from "../http/assemblyAPI";
import {fetchManufacturers} from "../http/manufacturerAPI";
import {Context} from "../index";
import {
    CASE_FAN_ROUTE,
    CASE_ROUTE,
    CPU_COOLER_ROUTE,
    CPU_ROUTE,
    GPU_ROUTE,
    MOTHERBOARD_ROUTE,
    POWER_SUPPLY_ROUTE,
    RAM_ROUTE,
    SSD_ROUTE
} from "../utils/consts";

const HomeAssemblyBuilder = observer(() => {
    const {assembly, parts, filtering, user} = useContext(Context)

    const [valid, setValid] = useState(false)

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data);
    }, [])

    useEffect(() => {
        let messages = []

        if (assembly.powerSupply && assembly.cpu_ && assembly.gpu) {
            const powerSupply = assembly.powerSupply;
            const cpu_ = assembly.cpu_;
            const gpu = assembly.gpu;

            const consumption = (cpu_.tdp + gpu.tdp) * 1.2
            if (consumption > powerSupply.maxPower) {
                messages.push(`Блок живлення є недостатньо потужним! Приблизне споживання системи  - ${Math.round(consumption)} W. Потужність блоку живлення ${powerSupply.maxPower} W`)
            }
        }

        if (assembly.ram && assembly.cpu_) {
            const ram = assembly.ram;
            const cpu_ = assembly.cpu_;

            if (cpu_.ramMaxClock < ram.clock) {
                messages.push(`Процесор підтримує максимум ${cpu_.ramMaxClock} MHz пам'ять, тому пам'ять буде працювати на цій частоті, не зважаючи на її максимальну частоту в ${ram.clock} MHz`);
            }
        }

        if (assembly.ram && assembly.cpuCooler) {
            const ram = assembly.ram;
            const cpuCooler = assembly.cpuCooler;

            if (cpuCooler.lowProfileRam !== ram.lowProfile) {
                messages.push(`Кулер процесору потребує оперативної пам'яті з низьким профілем, проте обрана є звичайною`);
            }
        }

        if (assembly.ram && assembly.motherboard && assembly.ramCount > 0) {
            const ram = assembly.ram;
            const motherboard = assembly.motherboard;
            const ramTotalCount = ram.size * assembly.ramCount;

            if (motherboard.ramMaxCapacity < ramTotalCount) {
                messages.push(`Материнська плата не підтримує обрану кіл-сть оперативної пам'яті. Максмальний об'єм, що підтримує материнка - ${motherboard.ramMaxCapacity}, загална кіл-сть обраної оперативки - ${ramTotalCount}`);
            }
        }

        if (assembly.ssd && assembly.motherboard) {
            const ssd = assembly.ssd;
            const motherboard = assembly.motherboard;

            if (ssd.ssdInterfaceId === 2 && motherboard.m2Count < 1) {
                messages.push(`Ви обрали SSD типу M.2 проте материнка не має жодного конектору для цього SSD`);
            }
        }

        if (assembly.gpu && assembly.case_) {
            const gpu = assembly.gpu;
            const case_ = assembly.case_;

            if (gpu.length > case_.innerLength) {
                messages.push(`Обрана відеокарта не вміститься в обраний корпус по довжині`);
            }
        }

        if (assembly.cpuCooler && assembly.case_) {
            const cpuCooler = assembly.cpuCooler;
            const case_ = assembly.case_;

            if (cpuCooler.height > case_.depth) {
                messages.push(`Обраний кулер процесору не вміститься в обраний корпус по висоті`);
            }
        }

        if (assembly.powerSupply && assembly.case_) {
            const powerSupply = assembly.powerSupply
            const case_ = assembly.case_;

            if (powerSupply.psuLength > case_.maxPsuLength) {
                messages.push(`Обраний блок живлення не вміститься в обраний корпус по довжині`);
            }
        }

        if (messages.length === 0 && assembly.cpu_ && assembly.cpuCooler && assembly.motherboard && assembly.ram && assembly.gpu && assembly.ssd && assembly.powerSupply && assembly.case_ && assembly.caseFan) setValid(true)
        assembly.messages = messages;
    }, [assembly._caseFan, assembly.caseFanCount, assembly.case_, assembly.cpuCooler, assembly.cpu_, assembly.gpu, assembly.motherboard, assembly.powerSupply, assembly.ram, assembly.ramCount, assembly.ssd])

    function handleCaseCoolerClear() {
        assembly.caseFan = null
        filtering.caseSelectedFanSize = {}
        filtering.caseSelectedFanSizeDisabled = false
    }

    function handleCaseClear() {
        assembly.case_ = null
        filtering.caseFanSelectedFanSize = {}
        filtering.caseFanSelectedFanSizeDisabled = false
        filtering.motherboardSelectedFormFactor = {}
        filtering.motherboardSelectedFormFactorDisabled = false
    }

    function handlePowerSupplyClear() {
        assembly.powerSupply = null
    }

    function handleSsdClear() {
        assembly.ssd = null
    }

    function handleGpuClear() {
        assembly.gpu = null
        filtering.motherboardSelectedBus = {}
        filtering.motherboardSelectedBusDisabled = false
    }

    function handleRamClear() {
        assembly.ram = null
        filtering.motherboardSelectedRamType = {}
        filtering.motherboardSelectedRamTypeDisabled = false
    }

    function handleMotherboardClear() {
        assembly.motherboard = null
        filtering.gpuSelectedBus = {}
        filtering.gpuSelectedBusDisabled = false
        filtering.ramSelectedRamType = {}
        filtering.ramSelectedRamTypeDisabled = false
        filtering.cpuSelectedSocket = {}
        filtering.cpuSelectedSocketDisabled = false
        filtering.cpuCoolerSelectedSocket = {}
        filtering.cpuCoolerSelectedSocketDisabled = false
        filtering.caseSelectedFormFactor = {}
        filtering.caseSelectedFormFactorDisabled = false
    }

    function handleCpuCoolerClear() {
        assembly.cpuCooler = null
        filtering.cpuSelectedSocket = {}
        filtering.cpuSelectedSocketDisabled = false
        filtering.motherboardSelectedSocket = {}
        filtering.motherboardSelectedSocketDisabled = false
    }

    function handleCpuClear() {
        assembly.cpu_ = null
        filtering.cpuCoolerSelectedSocket = {}
        filtering.cpuCoolerSelectedSocketDisabled = false
        filtering.motherboardSelectedSocket = {}
        filtering.motherboardSelectedSocketDisabled = false
    }

    function handleAssemblyPublish() {
        try {
            createAssembly({
                ramCount: assembly.ramCount,
                caseFanCount: assembly.caseFanCount,
                userId: user.user.id,
                caseId: assembly.case_.id,
                motherboardId: assembly.motherboard.id,
                cpuId: assembly.cpu_.id,
                cpuCoolerId: assembly.cpuCooler.id,
                ramId: assembly.ram.id,
                gpuId: assembly.gpu.id,
                ssdId: assembly.ssd.id,
                powerSupplyId: assembly.powerSupply.id,
                caseFanI: assembly.caseFan.id
            }).then(() => {
                assembly.caseFan = null
                assembly.caseFanCount = 0
                assembly.case_ = null
                assembly.cpuCooler = null
                assembly.cpu_ = null
                assembly.gpu = null
                assembly.motherboard = null
                assembly.powerSupply = null
                assembly.ram = null
                assembly.ramCount = 0
                assembly.ssd = null
            })
        } catch (e) {
            assembly.messages.push(e.message)
        }
    }

    function checkWarnings() {
        return !(assembly.messages = []);
    }

    return (<Card className="mb-5" style={{marginTop: 30 + 'px'}}>
        <Card.Body>
            <Card.Title className="display-5 mb-5">Конфігурація ПК</Card.Title>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>CPU (процесор)</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.cpu_ ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={CPU_ROUTE + '/' + assembly.cpu_.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.cpu_.manufacturerId).name} {assembly.cpu_.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleCpuClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={CPU_ROUTE}>Оберіть CPU</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>CPU кулер</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.cpuCooler ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={CPU_COOLER_ROUTE + '/' + assembly.cpuCooler.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.cpuCooler.manufacturerId).name} {assembly.cpuCooler.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleCpuCoolerClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={CPU_COOLER_ROUTE}>Оберіть CPU кулер</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Материнська плата</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.motherboard ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={MOTHERBOARD_ROUTE + '/' + assembly.motherboard.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.motherboard.manufacturerId).name} {assembly.motherboard.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleMotherboardClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={MOTHERBOARD_ROUTE}>Оберіть материнську плату</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Пам'ять</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.ram ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={RAM_ROUTE + '/' + assembly.ram.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.ram.manufacturerId).name} {assembly.ram.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    {assembly.motherboard && assembly.ramCount > 0 ? <Col>
                                        <Dropdown className="mt-2 mb-2">
                                            <Dropdown.Toggle>{assembly.ramCount || 'Кіл-сть ОЗУ'}</Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                {Array.from({length: assembly.motherboard.ramCount}, (_, i) => i + 1).map(num =>
                                                    <Dropdown.Item
                                                        key={num}
                                                        onClick={() => assembly.ramCount = num}>
                                                        {num}</Dropdown.Item>)}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </Col> : ''}
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleRamClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={RAM_ROUTE}>Оберіть пам'ять</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>GPU (відео-карта)</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.gpu ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={GPU_ROUTE + '/' + assembly.gpu.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.gpu.manufacturerId).name} {assembly.gpu.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleGpuClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={GPU_ROUTE}>Оберіть GPU</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Накопичувач</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.ssd ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={SSD_ROUTE + '/' + assembly.ssd.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.ssd.manufacturerId).name} {assembly.ssd.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleSsdClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={SSD_ROUTE}>Оберіть накопичувач</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Блок живлення</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.powerSupply ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={POWER_SUPPLY_ROUTE + '/' + assembly.powerSupply.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.powerSupply.manufacturerId).name} {assembly.powerSupply.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handlePowerSupplyClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={POWER_SUPPLY_ROUTE}>Оберіть блок живлення</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Корпус</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.case_ ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={CASE_ROUTE + '/' + assembly.case_.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.case_.manufacturerId).name} {assembly.case_.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleCaseClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={CASE_ROUTE}>Оберіть корпус</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-4 col-12">
                    <Card.Text>Кулер корпусу</Card.Text>
                </Col>
                <Col className="col-lg-8 col-12 mb-2">
                    <Card>
                        <Card.Body>
                            {assembly.caseFan ? <>
                                <Row>
                                    <Col>
                                        <Card.Text>
                                            <NavLink
                                                to={CASE_FAN_ROUTE + '/' + assembly.caseFan.id}>{parts.manufacturers.find(manufacturer => manufacturer.id === assembly.caseFan.manufacturerId).name} {assembly.caseFan.name}
                                            </NavLink>
                                        </Card.Text>
                                    </Col>
                                    {assembly.case_ && assembly.caseFanCount > 0 ? <Col>
                                        <Dropdown className="mt-2 mb-2">
                                            <Dropdown.Toggle>{assembly.caseFanCount || 'Кіл-сть вентиляторів'}</Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                {Array.from({length: assembly.case_.fanCount}, (_, i) => i + 1).map(num =>
                                                    <Dropdown.Item
                                                        key={num}
                                                        onClick={() => assembly.caseFanCount = num}>
                                                        {num}</Dropdown.Item>)}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </Col> : ''}
                                    <Col>
                                        <Button
                                            onClick={() => {
                                                handleCaseCoolerClear()
                                            }}>
                                            Очистити
                                        </Button>
                                    </Col>
                                </Row></> : <NavLink to={CASE_FAN_ROUTE}>Оберіть кулер корпусу</NavLink>}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                {assembly.messages.map(message => <Alert variant="warning" key={Math.random() * 10}>
                    {message}
                </Alert>)}
            </Row>
            {user.isAuth ? <Row className="d-flex flex-row-reverse">
                <Button className="w-25" disabled={!valid} onClick={handleAssemblyPublish}>Опублікувати</Button>
            </Row> : ''}

        </Card.Body>
    </Card>);
});

export default HomeAssemblyBuilder;