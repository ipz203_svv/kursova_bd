import {
    CategoryScale,
    Chart as ChartJS,
    Legend,
    LinearScale,
    LineElement,
    PointElement,
    Title,
    Tooltip,
} from 'chart.js'
import React, {useState} from 'react';
import {Container} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {Line} from "react-chartjs-2";
import {GetUsers} from "../../http/statsAPI";


ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend)

const UsersStats = () => {

    const [usersMonth1, setUsersMonth1] = useState(0)
    const [usersMonth2, setUsersMonth2] = useState(0)
    const [usersMonth3, setUsersMonth3] = useState(0)
    const [usersMonth4, setUsersMonth4] = useState(0)
    const [usersMonth5, setUsersMonth5] = useState(0)
    const [usersMonth6, setUsersMonth6] = useState(0)
    const [usersMonth7, setUsersMonth7] = useState(0)
    const [usersMonth8, setUsersMonth8] = useState(0)
    const [usersMonth9, setUsersMonth9] = useState(0)
    const [usersMonth10, setUsersMonth10] = useState(0)
    const [usersMonth11, setUsersMonth11] = useState(0)
    const [usersMonth12, setUsersMonth12] = useState(0)


    GetUsers(1).then(data => setUsersMonth1(data[0][0].count))
    GetUsers(2).then(data => setUsersMonth2(data[0][0].count))
    GetUsers(3).then(data => setUsersMonth3(data[0][0].count))
    GetUsers(4).then(data => setUsersMonth4(data[0][0].count))
    GetUsers(5).then(data => setUsersMonth5(data[0][0].count))
    GetUsers(6).then(data => setUsersMonth6(data[0][0].count))
    GetUsers(7).then(data => setUsersMonth7(data[0][0].count))
    GetUsers(8).then(data => setUsersMonth8(data[0][0].count))
    GetUsers(9).then(data => setUsersMonth9(data[0][0].count))
    GetUsers(10).then(data => setUsersMonth10(data[0][0].count))
    GetUsers(11).then(data => setUsersMonth11(data[0][0].count))
    GetUsers(12).then(data => setUsersMonth12(data[0][0].count))


    const data = {
        labels: ['січень', 'лютий', 'березень', 'квітень', 'травень', 'червень', 'липень', 'серпень', 'вересень', 'жовтень', 'листопад', 'грудень'],
        datasets: [{
            label: 'Реєстрація користувачів в місяць',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgb(177,255,203)',
            borderColor: 'rgb(90,255,166)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgb(65,255,159)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgb(165,255,203)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [usersMonth1, usersMonth2, usersMonth3, usersMonth4, usersMonth5, usersMonth6, usersMonth7, usersMonth8, usersMonth9, usersMonth10, usersMonth11, usersMonth12]
        }]
    };


    return (<Container style={{marginTop: 30 + 'px'}}>
        <Row className="align-content-center">
            <Col md={9}>
                <h3>Нові користувачі помісячно</h3>
                <Line data={data}/>
            </Col>
        </Row>
    </Container>);
}

export default UsersStats;