import {
    CategoryScale,
    Chart as ChartJS,
    Legend,
    LinearScale,
    LineElement,
    PointElement,
    Title,
    Tooltip,
} from 'chart.js'
import React, {useState} from 'react';
import {Container} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {Line} from "react-chartjs-2";
import {GetAssemblies} from "../../http/statsAPI";


ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend)

const UsersStats = () => {

    const [assembliesMonth1, setAssembliesMonth1] = useState(0)
    const [assembliesMonth2, setAssembliesMonth2] = useState(0)
    const [assembliesMonth3, setAssembliesMonth3] = useState(0)
    const [assembliesMonth4, setAssembliesMonth4] = useState(0)
    const [assembliesMonth5, setAssembliesMonth5] = useState(0)
    const [assembliesMonth6, setAssembliesMonth6] = useState(0)
    const [assembliesMonth7, setAssembliesMonth7] = useState(0)
    const [assembliesMonth8, setAssembliesMonth8] = useState(0)
    const [assembliesMonth9, setAssembliesMonth9] = useState(0)
    const [assembliesMonth10, setAssembliesMonth10] = useState(0)
    const [assembliesMonth11, setAssembliesMonth11] = useState(0)
    const [assembliesMonth12, setAssembliesMonth12] = useState(0)


    GetAssemblies(1).then(data => setAssembliesMonth1(data[0][0].count))
    GetAssemblies(2).then(data => setAssembliesMonth2(data[0][0].count))
    GetAssemblies(3).then(data => setAssembliesMonth3(data[0][0].count))
    GetAssemblies(4).then(data => setAssembliesMonth4(data[0][0].count))
    GetAssemblies(5).then(data => setAssembliesMonth5(data[0][0].count))
    GetAssemblies(6).then(data => setAssembliesMonth6(data[0][0].count))
    GetAssemblies(7).then(data => setAssembliesMonth7(data[0][0].count))
    GetAssemblies(8).then(data => setAssembliesMonth8(data[0][0].count))
    GetAssemblies(9).then(data => setAssembliesMonth9(data[0][0].count))
    GetAssemblies(10).then(data => setAssembliesMonth10(data[0][0].count))
    GetAssemblies(11).then(data => setAssembliesMonth11(data[0][0].count))
    GetAssemblies(12).then(data => setAssembliesMonth12(data[0][0].count))


    const data = {
        labels: ['січень', 'лютий', 'березень', 'квітень', 'травень', 'червень', 'липень', 'серпень', 'вересень', 'жовтень', 'листопад', 'грудень'],
        datasets: [{
            label: 'Додано нових збірок в місяць',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgb(255,222,175)',
            borderColor: 'rgb(196,139,28)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgb(199,85,14)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgb(252,162,51)',
            pointHoverBorderColor: 'rgb(255,202,125)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [assembliesMonth1, assembliesMonth2, assembliesMonth3, assembliesMonth4, assembliesMonth5, assembliesMonth6, assembliesMonth7, assembliesMonth8, assembliesMonth9, assembliesMonth10, assembliesMonth11, assembliesMonth12]
        }]
    };


    return (<Container style={{marginTop: 30 + 'px'}}>
        <Row className="align-content-center">
            <Col md={9}>
                <h3>Нові збірки помісячно</h3>
                <Line data={data}/>
            </Col>
        </Row>
    </Container>);
}

export default UsersStats;