import React, {useContext, useEffect, useState} from 'react';
import {Button, Card, Col, Row} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {deleteAssembly} from "../http/assemblyAPI";
import {fetchCpu} from "../http/cpuAPI";
import {fetchGpu} from "../http/gpuAPI";
import {createLike, deleteLike, fetchLike, fetchLikesCount} from "../http/likeAPI";
import {fetchManufacturers} from "../http/manufacturerAPI";
import {fetchRam} from "../http/ramAPI";
import {fetchRamTypes} from "../http/ramTypeAPI";
import {Context} from "../index";
import {ASSEMBLY_ROUTE} from "../utils/consts";

const AssemblyComponent = (assembly_) => {
    const navigate = useNavigate();
    const assembly = assembly_.assembly
    const {parts, user} = useContext(Context)

    const [cpu, setCpu] = useState({})
    const [cpuLoading, setCpuLoading] = useState(true)
    const [gpu, setGpu] = useState({})
    const [gpuLoading, setGpuLoading] = useState(true)
    const [ram, setRam] = useState({})
    const [ramLoading, setRamLoading] = useState(true)
    const [manufacturersLoading, setManufacturersLoading] = useState(true)
    const [ramTypesLoading, setRamTypesLoading] = useState(true)
    const [likesCount, setLikesCount] = useState(0)
    const [likesCountLoading, setLikesCountLoading] = useState(true)
    const [like, setLike] = useState(null)
    const [likeLoading, setlikeLoading] = useState(true)


    useEffect(() => {
        fetchManufacturers().then(data => {
            parts.manufacturers = data
            setManufacturersLoading(false)
        })
        fetchRamTypes().then(data => {
            parts.ramTypes = data
            setRamTypesLoading(false)
        })
        fetchCpu(assembly.cpuId || 1).then(data => {
            setCpu(data)
            setCpuLoading(false)
        })
        fetchGpu(assembly.gpuId || 1).then(data => {
            setGpu(data)
            setGpuLoading(false)
        })
        fetchRam(assembly.ramId || 1).then(data => {
            setRam(data)
            setRamLoading(false)
        })
        fetchLikesCount(assembly.id).then(data => {
            setLikesCount(data)
            setLikesCountLoading(false)
        })
        fetchLike(user.user.id, assembly.id).then(data => {
            setLike(data)
            setlikeLoading(false)
        })
    }, [])

    useEffect(() => {
        fetchLikesCount(assembly.id).then(data => {
            setLikesCount(data)
            setLikesCountLoading(false)
        })
    }, [like])

    useEffect(() => {
        fetchLike(user.user.id, assembly.id).then(data => {
            setLike(data)
            setlikeLoading(false)
        })
    }, [likesCount])

    function handleCreateLike() {
        createLike({
            userId: user.user.id, assemblyId: assembly.id
        }).then(data => setLike(data))
    }

    function handleDeleteLike() {
        deleteLike(like.id).then(data => setLike({}))
    }

    if (ramTypesLoading || manufacturersLoading || cpuLoading || gpuLoading || ramLoading || likeLoading || likesCountLoading) {
        return <div>Loading...</div>
    }

    return (<Card>
        <Card.Body>
            <Row>
                <Col>
                    Процесор:
                </Col>
                <Col>
                    {parts.manufacturers.find(m => m.id === cpu.manufacturerId).name} {cpu.name}
                </Col>
            </Row>
            <Row>
                <Col>
                    Відео-карта:
                </Col>
                <Col>
                    {parts.manufacturers.find(m => m.id === gpu.manufacturerId).name} {gpu.name}
                </Col>
            </Row>
            <Row>
                <Col>
                    Оперативна пам'ять:
                </Col>
                <Col>
                    {parseInt(ram.size) * parseInt(assembly.ramCount)} Mb {parts.ramTypes.find(m => m.id === ram.ramTypeId).name} {parts.manufacturers.find(m => m.id === ram.manufacturerId).name} {ram.name}
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col className="d-flex flex-row-reverse">
                    {user.isAdmin() ? <Button className="w-25" variant="warning"
                                              onClick={() => deleteAssembly(assembly.id)}>Видалити</Button> : ''}
                    <Button className="w-25"
                            onClick={() => navigate(ASSEMBLY_ROUTE + '/' + assembly.id)}>Детальніше</Button>
                    {like ? <Button className="w-50" onClick={() => handleDeleteLike()}>Видалити вподобайку</Button> :
                        <Button className="w-50" onClick={() => handleCreateLike()}>Вподобати</Button>}
                    Вподобайок: {likesCount}
                </Col>
            </Row>
        </Card.Body>
    </Card>);
};

export default AssemblyComponent;