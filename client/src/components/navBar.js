import {observer} from "mobx-react-lite";
import React, {useContext} from 'react';
import {Button, Container, Nav, Navbar, NavLink} from "react-bootstrap";
import {useLocation, useNavigate} from "react-router-dom";
import {Context} from "../index";
import {ADMIN_ROUTE, ASSEMBLY_ROUTE, HOME_ROUTE, LOGIN_ROUTE, PROFILE_ROUTE, REGISTRATION_ROUTE} from "../utils/consts";
import {logOut} from "../utils/logOut"

const NavBar = observer(() => {
    const {user} = useContext(Context)
    const navigate = useNavigate()
    const route = useLocation()
    const onAuthPage = route.pathname === LOGIN_ROUTE || route.pathname === REGISTRATION_ROUTE

    return (<Navbar bg="dark" variant="dark">
        <Container>
            <NavLink className="ms-2 text-light"
                     onClick={() => navigate(HOME_ROUTE)}>PCbuilder{route.pathname}</NavLink>
            {user.isAuth ? <NavLink className="ms-2 text-light"
                     onClick={() => navigate(ASSEMBLY_ROUTE)}>Готові збірки</NavLink> : ''}
            {user.isAuth ? user.isAdmin() ? <Nav className="ms-auto">
                <Button variant={"light"} onClick={() => navigate(ADMIN_ROUTE)}>Адмін меню</Button>
            </Nav> : "" : ""}
            {onAuthPage ? "" : user.isAuth ? <Nav className="ms-auto">
                <NavLink onClick={() => navigate(PROFILE_ROUTE)}
                         className="link-light">{user.user.email}</NavLink>
                <Button variant={"light"} onClick={() => logOut(user)}>Вийти</Button>
            </Nav> : <Nav className="ms-auto">
                <Button variant={"light"} onClick={() => navigate(LOGIN_ROUTE)}>Увійти</Button>
            </Nav>}
        </Container>
    </Navbar>);
});

export default NavBar;