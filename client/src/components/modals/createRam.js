import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {createRam} from "../../http/ramAPI";
import {fetchRamTypes} from "../../http/ramTypeAPI";
import {Context} from "../../index";

const CreateRam = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [size, setSize] = useState(1);
    const [clock, setClock] = useState(1);
    const [lowProfile, setLowProfile] = useState(false);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedRamType, setSelectedRamType] = useState({});

    useEffect(() => {
        fetchRamTypes().then(data => parts.ramTypes = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateRam = () => {
        createRam({
            name: name,
            releaseYear: releaseYear,
            size: size,
            clock: clock,
            lowProfile: lowProfile,
            manufacturerId: selectedManufacturer.id,
            ramTypeId: selectedRamType.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setSize(1)
            setClock(1)
            setLowProfile(false)
            setSelectedManufacturer({})
            setSelectedRamType({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання оперативної пам'яті</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Тип оперативної пам'яті</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedRamType.name || 'Оберіть тип'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.ramTypes.map(ramType => <Dropdown.Item
                                key={ramType.id}
                                onClick={() => setSelectedRamType(ramType)}
                            >{ramType.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Величина</Form.Label>
                    <Form.Control
                        type="number"
                        value={size}
                        onChange={e => setSize(parseInt(e.target.value))}
                        placeholder={'Величина MB (напр. 8096)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Частота</Form.Label>
                    <Form.Control
                        type="number"
                        value={clock}
                        onChange={e => setClock(parseInt(e.target.value))}
                        placeholder={'Частота MHz (напр. 3600)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Має низький профіль?</Form.Label>
                    <Form.Check
                        type="switch"
                        isValid={lowProfile}
                        onChange={e => setLowProfile(e.target.checked)}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateRam}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateRam;