import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {createCase} from "../../http/caseAPI";
import {fetchFanSizes} from "../../http/fanSizeAPI";
import {fetchFormFactors} from "../../http/formFactorAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";

const CreateCase = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [width, setWidth] = useState(1.0);
    const [height, setHeight] = useState(1.0);
    const [innerLength, setInnerLength] = useState(1.0);
    const [depth, setDepth] = useState(1.0);
    const [maxPsuLength, setMaxPsuLength] = useState(1.0);
    const [fanCount, setFanCount] = useState(1);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedFormFactor, setSelectedFormFactor] = useState({});
    const [selectedFanSize, setSelectedFanSize] = useState({});

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchFormFactors().then(data => parts.formFactors = data)
        fetchFanSizes().then(data => parts.fanSizes = data)

    }, [])

    const handleCreateCase = () => {
        createCase({
            name: name,
            releaseYear: releaseYear,
            width: width,
            height: height,
            innerLength: innerLength,
            depth: depth,
            maxPsuLength: maxPsuLength,
            fanCount: fanCount,
            manufacturerId: selectedManufacturer.id,
            formFactorId: selectedFormFactor.id,
            fanSizeId: selectedFanSize.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setWidth(1.0)
            setHeight(1.0)
            setInnerLength(1.0)
            setDepth(1.0)
            setMaxPsuLength(1.0)
            setFanCount(1)
            setSelectedManufacturer({})
            setSelectedFormFactor({})
            setSelectedFanSize({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання корпусу</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Форм-фактор</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedFormFactor.name || 'Оберіть форм-фактор'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.formFactors.map(formFactor => <Dropdown.Item
                                key={formFactor.id}
                                onClick={() => setSelectedFormFactor(formFactor)}
                            >{formFactor.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Розмір вентилятору</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedFanSize.size || 'Оберіть розмір вентилятору'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.fanSizes.map(fanSize => <Dropdown.Item
                                key={fanSize.id}
                                onClick={() => setSelectedFanSize(fanSize)}
                            >{fanSize.size}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Ширина</Form.Label>
                    <Form.Control
                        type="number"
                        value={width}
                        onChange={e => setWidth(parseFloat(e.target.value))}
                        placeholder={'Ширина mm (напр. 100)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Висота</Form.Label>
                    <Form.Control
                        type="number"
                        value={height}
                        onChange={e => setHeight(parseFloat(e.target.value))}
                        placeholder={'Висота mm (напр. 300)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Внутрішня довжина</Form.Label>
                    <Form.Control
                        type="number"
                        value={innerLength}
                        onChange={e => setInnerLength(parseFloat(e.target.value))}
                        placeholder={'Внутрішня довжина mm (напр. 400)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Глубина</Form.Label>
                    <Form.Control
                        type="number"
                        value={depth}
                        onChange={e => setDepth(parseFloat(e.target.value))}
                        placeholder={'Глубина mm (напр. 80)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Максимальна довжина блоку живлення</Form.Label>
                    <Form.Control
                        type="number"
                        value={maxPsuLength}
                        onChange={e => setMaxPsuLength(parseFloat(e.target.value))}
                        placeholder={'Максимальна довжина psu mm (напр. 70)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Можлива кількість вентиляторів</Form.Label>
                    <Form.Control
                        type="number"
                        value={fanCount}
                        onChange={e => setFanCount(parseInt(e.target.value))}
                        placeholder={'Можлива кількість вентиляторів (напр. 6)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateCase}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateCase;