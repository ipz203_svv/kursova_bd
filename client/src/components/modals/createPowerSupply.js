import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {createPowerSupply} from "../../http/powerSupplyAPI";
import {Context} from "../../index";

const CreatePowerSupply = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [maxPower, setMaxPower] = useState(1);
    const [rating80Plus, setRating80Plus] = useState('');
    const [psuLength, setPsuLength] = useState(1.0);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});

    useEffect(() => {
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreatePowerSupply = () => {
        createPowerSupply({
            name: name,
            releaseYear: releaseYear,
            maxPower: maxPower,
            rating80Plus: rating80Plus,
            psuLength: psuLength,
            manufacturerId: selectedManufacturer.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setMaxPower(1)
            setRating80Plus('')
            setPsuLength(1.0)
            setSelectedManufacturer({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання блоку живлення</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Максимальне навантаження</Form.Label>
                    <Form.Control
                        type="number"
                        value={maxPower}
                        onChange={e => setMaxPower(parseInt(e.target.value))}
                        placeholder={'Максимальне навантаження W (напр. 600)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рейтинг 80plus</Form.Label>
                    <Form.Control
                        value={rating80Plus}
                        onChange={e => setRating80Plus(e.target.value)}
                        placeholder={'Рейтинг 80plus (напр. Bronze)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Довжина</Form.Label>
                    <Form.Control
                        type="number"
                        value={psuLength}
                        onChange={e => setPsuLength(parseFloat(e.target.value))}
                        placeholder={'Довжина (напр. 80)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreatePowerSupply}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreatePowerSupply;