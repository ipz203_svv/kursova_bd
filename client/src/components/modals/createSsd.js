import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {createSsd} from "../../http/ssdAPI";
import {fetchSsdInterfaces} from "../../http/ssdInterfaceAPI";
import {Context} from "../../index";

const CreateSsd = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [size, setSize] = useState(1);
    const [readSpeed, setReadSpeed] = useState('');
    const [writeSpeed, setWriteSpeed] = useState('');
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedSsd, setSelectedSsd] = useState({});

    useEffect(() => {
        fetchSsdInterfaces().then(data => parts.ssdInterfaces = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateSsd = () => {
        createSsd({
            name: name,
            releaseYear: releaseYear,
            size: size,
            readSpeed: readSpeed,
            writeSpeed: writeSpeed,
            manufacturerId: selectedManufacturer.id,
            ssdInterfaceId: selectedSsd.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setSize(1)
            setWriteSpeed('')
            setReadSpeed('')
            setSelectedManufacturer({})
            setSelectedSsd({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання процесору</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Інтерфейс</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedSsd.name || 'Оберіть інтерфейс'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.ssdInterfaces.map(ssdInterface => <Dropdown.Item
                                key={ssdInterface.id}
                                onClick={() => setSelectedSsd(ssdInterface)}
                            >{ssdInterface.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Об'єм</Form.Label>
                    <Form.Control
                        type="number"
                        value={size}
                        onChange={e => setSize(parseInt(e.target.value))}
                        placeholder={'Об\'єм GB (напр. 500)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Швидкість читання</Form.Label>
                    <Form.Control
                        value={readSpeed}
                        onChange={e => setReadSpeed(e.target.value)}
                        placeholder={'Швидкість читання MB/s (напр. 2500)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Швидкість запису</Form.Label>
                    <Form.Control
                        value={writeSpeed}
                        onChange={e => setWriteSpeed(e.target.value)}
                        placeholder={'Швидкість запису MB/s (напр. 2000)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateSsd}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateSsd;