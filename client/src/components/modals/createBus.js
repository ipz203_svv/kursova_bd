import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createBus} from "../../http/busAPI";

const CreateBus = ({show, onHide}) => {
    const [value, setValue] = useState('');

    const handleCreateBus = () => {
        createBus({name: value}).then(() => {
                setValue('')
                onHide()
            }
        )
    }

    return (
        <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Додавання розрядності PCI конектору</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={'Розрядність (напр. x16)'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Відмінити
                </Button>
                <Button variant="primary" onClick={handleCreateBus}>
                    Зберегти
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateBus;