import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {createCpuCooler} from "../../http/cpuCoolerAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchSockets} from "../../http/socketAPI";
import {Context} from "../../index";

const CreateCpuCooler = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [height, setHeight] = useState(1.0);
    const [lowProfileRam, setLowProfileRam] = useState(false);
    const [tdp, setTdp] = useState(1);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedSocket, setSelectedSocket] = useState({});

    useEffect(() => {
        fetchSockets().then(data => parts.sockets_ = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateCpuCooler = () => {
        createCpuCooler({
            name: name,
            releaseYear: releaseYear,
            height: height,
            lowProfileRam: lowProfileRam,
            tdp: tdp,
            manufacturerId: selectedManufacturer.id,
            socketId: selectedSocket.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setHeight(0.1)
            setLowProfileRam(false)
            setTdp(1)
            setSelectedManufacturer({})
            setSelectedSocket({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання кулеру процесору</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Сокет</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.sockets_.map(socket => <Dropdown.Item
                                key={socket.id}
                                onClick={() => setSelectedSocket(socket)}
                            >{socket.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Висота</Form.Label>
                    <Form.Control
                        type="number"
                        value={height}
                        onChange={e => setHeight(parseFloat(e.target.value))}
                        placeholder={'Висота mm (напр. 60)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть тепла, що може розсіювати кулер</Form.Label>
                    <Form.Control
                        type="number"
                        value={tdp}
                        onChange={e => setTdp(parseInt(e.target.value))}
                        placeholder={'Кіл-сть потоків (напр. 16)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Чи потрібно використовувати оперативну пам'ять з низьким профілем?</Form.Label>
                    <Form.Check
                        type="switch"
                        isValid={lowProfileRam}
                        onChange={e => setLowProfileRam(e.target.checked)}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateCpuCooler}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateCpuCooler;