import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {fetchBuses} from "../../http/busAPI";
import {fetchFormFactors} from "../../http/formFactorAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {createMotherboard} from "../../http/motherboardAPI";
import {fetchRamTypes} from "../../http/ramTypeAPI";
import {fetchSockets} from "../../http/socketAPI";
import {fetchSsdInterfaces} from "../../http/ssdInterfaceAPI";
import {Context} from "../../index";

const CreateMotherboard = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [chipset, setChipset] = useState('');
    const [ramCount, setRamCount] = useState(4);
    const [ramMaxCapacity, setRamMaxCapacity] = useState(1);
    const [m2Count, setM2Count] = useState(1);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedSocket, setSelectedSocket] = useState({});
    const [selectedFormFactor, setSelectedFormFactor] = useState({});
    const [selectedRamType, setSelectedRamType] = useState({});
    const [selectedSsdInterface, setSelectedSsdInterface] = useState({});
    const [selectedBus, setSelectedBus] = useState({});

    useEffect(() => {
        fetchSockets().then(data => parts.sockets_ = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
        fetchFormFactors().then(data => parts.formFactors = data)
        fetchRamTypes().then(data => parts.ramTypes = data)
        fetchSsdInterfaces().then(data => parts.ssdInterfaces = data)
        fetchBuses().then(data => parts.buses = data)
    }, [])

    const handleCreateMotherboard = () => {
        createMotherboard({
            name: name,
            releaseYear: releaseYear,
            chipset: chipset,
            ramCount: ramCount,
            ramMaxCapacity: ramMaxCapacity,
            m2Count: m2Count,
            manufacturerId: selectedManufacturer.id,
            socketId: selectedSocket.id,
            formFactorId: selectedFormFactor.id,
            ramTypeId: selectedRamType.id,
            ssdInterfaceId: selectedSsdInterface.id,
            busId: selectedBus.id
        }).then(() => {
            setName('')
            setReleaseYear('')
            setChipset('')
            setRamCount(4)
            setRamMaxCapacity(1)
            setM2Count(1)
            setSelectedManufacturer({})
            setSelectedSocket({})
            setSelectedFormFactor({})
            setSelectedRamType({})
            setSelectedSsdInterface({})
            setSelectedBus({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання материнскої плати</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Сокет</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.sockets_.map(socket => <Dropdown.Item
                                key={socket.id}
                                onClick={() => setSelectedSocket(socket)}
                            >{socket.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Форм-фактор</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedFormFactor.name || 'Оберіть форм-фактор'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.formFactors.map(formFactor => <Dropdown.Item
                                key={formFactor.id}
                                onClick={() => setSelectedFormFactor(formFactor)}
                            >{formFactor.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Тип оперативної пам'яті</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedRamType.name || 'Оберіть тип оперативної пам\'яті'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.ramTypes.map(ramType => <Dropdown.Item
                                key={ramType.id}
                                onClick={() => setSelectedRamType(ramType)}
                            >{ramType.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Інтерфейс підключення SSD</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedSsdInterface.name || 'Оберіть інтерфейс підключення SSD'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.ssdInterfaces.map(ssdInterface => <Dropdown.Item
                                key={ssdInterface.id}
                                onClick={() => setSelectedSsdInterface(ssdInterface)}
                            >{ssdInterface.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Розрядність PCI конектору</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedBus.name || 'Оберіть розрядність PCI конектору'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.buses.map(bus => <Dropdown.Item
                                key={bus.id}
                                onClick={() => setSelectedBus(bus)}
                            >{bus.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Чіпсет</Form.Label>
                    <Form.Control
                        value={chipset}
                        onChange={e => setChipset(e.target.value)}
                        placeholder={'Чіпсет (напр. B550)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть планок оперативної пам'яті</Form.Label>
                    <Form.Control
                        type="number"
                        value={ramCount}
                        onChange={e => setRamCount(parseInt(e.target.value))}
                        placeholder={'Кіл-сть планок оперативної пам\'яті (напр. 4)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Максимальний об'єм оперативної пам'яті</Form.Label>
                    <Form.Control
                        type="number"
                        value={ramMaxCapacity}
                        onChange={e => setRamMaxCapacity(parseInt(e.target.value))}
                        placeholder={'Частота (напр. 2.8)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть слотів M.2</Form.Label>
                    <Form.Control
                        type="number"
                        value={m2Count}
                        onChange={e => setM2Count(parseInt(e.target.value))}
                        placeholder={'Кіл-сть слотів M.2 (напр. 2)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateMotherboard}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateMotherboard;