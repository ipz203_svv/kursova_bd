import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {createCaseFan} from "../../http/caseFanAPI";
import {fetchFanSizes} from "../../http/fanSizeAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";

const CreateCaseFan = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [rpm, setRpm] = useState('');
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedFanSize, setSelectedFanSize] = useState({});

    useEffect(() => {
        fetchFanSizes().then(data => parts.fanSizes = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateCaseFan = () => {
        createCaseFan({
            name: name,
            releaseYear: releaseYear,
            rpm: rpm,
            manufacturerId: selectedManufacturer.id,
            fanSizeId: selectedFanSize.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setRpm('')
            setSelectedManufacturer({})
            setSelectedFanSize({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання вентилятору корпуса</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Розмір</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedFanSize.size || 'Оберіть розмір'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.fanSizes.map(fanSize => <Dropdown.Item
                                key={fanSize.id}
                                onClick={() => setSelectedFanSize(fanSize)}
                            >{fanSize.size}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Швидкість обертання</Form.Label>
                    <Form.Control
                        value={rpm}
                        onChange={e => setRpm(e.target.value)}
                        placeholder={'Швидкість обертання (напр. 1800)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateCaseFan}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateCaseFan;