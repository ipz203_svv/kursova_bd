import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {fetchBuses} from "../../http/busAPI";
import {createGpu} from "../../http/gpuAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {Context} from "../../index";

const CreateGpu = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [chip, setChip] = useState('');
    const [memorySize, setMemorySize] = useState(1);
    const [gpuClock, setGpuClock] = useState('');
    const [memoryClock, setMemoryClock] = useState('');
    const [length, setLength] = useState(1);
    const [tdp, setTdp] = useState(1);
    const [gmemoryType, setGmemoryType] = useState('');
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedBus, setSelectedBus] = useState({});

    useEffect(() => {
        fetchBuses().then(data => parts.buses = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateGpu = () => {
        createGpu({
            name: name,
            releaseYear: releaseYear,
            chip: chip,
            gpuClock: gpuClock,
            memoryClock: memoryClock,
            memorySize: memorySize,
            length: length,
            gmemoryType: gmemoryType,
            tdp: tdp,
            manufacturerId: selectedManufacturer.id,
            busId: selectedBus.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setChip('')
            setMemorySize(1)
            setGpuClock('')
            setMemoryClock('')
            setLength(1)
            setGmemoryType('')
            setTdp(1)
            setSelectedManufacturer({})
            setSelectedBus({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання відеокарти</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer => <Dropdown.Item
                                key={manufacturer.id}
                                onClick={() => setSelectedManufacturer(manufacturer)}
                            >{manufacturer.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Сокет</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedBus.name || 'Оберіть розрядність PCI конектору'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.buses.map(bus => <Dropdown.Item
                                key={bus.id}
                                onClick={() => setSelectedBus(bus)}
                            >{bus.name}</Dropdown.Item>)}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Графічний чіп</Form.Label>
                    <Form.Control
                        value={chip}
                        onChange={e => setChip(e.target.value)}
                        placeholder={'Назва Чіпу'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть відео-пам'яті</Form.Label>
                    <Form.Control
                        type="number"
                        value={memorySize}
                        onChange={e => setMemorySize(parseInt(e.target.value))}
                        placeholder={'Кіл-сть відео-пам\'яті (напр. 2024)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Тип відео-пам'яті</Form.Label>
                    <Form.Control
                        value={gmemoryType}
                        onChange={e => setGmemoryType(e.target.value)}
                        placeholder={'Тип пам\'яті (напр. GDDR5)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Частота чіпу</Form.Label>
                    <Form.Control
                        value={gpuClock}
                        onChange={e => setGpuClock(e.target.value)}
                        placeholder={'Частота (напр. 1920)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Частота пам'яті</Form.Label>
                    <Form.Control
                        value={memoryClock}
                        onChange={e => setMemoryClock(e.target.value)}
                        placeholder={'Частота (напр. 1180)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть тепла, що порібно розсіювати (напр. 75)</Form.Label>
                    <Form.Control
                        type="number"
                        value={tdp}
                        onChange={e => setTdp(parseInt(e.target.value))}
                        placeholder={'Кіл-сть тепла, що порібно розсіювати (напр. 180)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Довжина відеокарти</Form.Label>
                    <Form.Control
                        type="number"
                        value={length}
                        onChange={e => setLength(parseFloat(e.target.value))}
                        placeholder={'Довжина мм (напр. 270)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateGpu}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateGpu;