import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createFormFactor} from "../../http/formFactorAPI";

const CreateFormFactor = ({show, onHide}) => {
    const [value, setValue] = useState('');

    const handleCreateFormFactor = () => {
        createFormFactor({name: value}).then(() => {
                setValue('')
                onHide()
            }
        )
    }

    return (
        <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Додавання форм-фактору (мат. плата -
                    корпус)</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={'Назва форм-фактору (напр. ATX)'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Відмінити
                </Button>
                <Button variant="primary" onClick={handleCreateFormFactor}>
                    Зберегти
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateFormFactor;