import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Form, Modal} from "react-bootstrap";
import {createCpu} from "../../http/cpuAPI";
import {fetchManufacturers} from "../../http/manufacturerAPI";
import {fetchSockets} from "../../http/socketAPI";
import {Context} from "../../index";

const CreateCpu = ({show, onHide}) => {
    const {parts} = useContext(Context)
    const [name, setName] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [core, setCore] = useState('');
    const [thread, setThread] = useState('');
    const [clock, setClock] = useState('');
    const [process, setProcess] = useState('');
    const [cache, setCache] = useState('');
    const [ramMaxClock, setRamMaxClock] = useState(1);
    const [tdp, setTdp] = useState(1);
    const [selectedManufacturer, setSelectedManufacturer] = useState({});
    const [selectedSocket, setSelectedSocket] = useState({});

    useEffect(() => {
        fetchSockets().then(data => parts.sockets_ = data)
        fetchManufacturers().then(data => parts.manufacturers = data)
    }, [])

    const handleCreateCpu = () => {
        createCpu({
            name: name,
            releaseYear: releaseYear,
            core: core,
            thread: thread,
            clock: clock,
            process: process,
            cache: cache,
            ramMaxClock: ramMaxClock,
            tdp: tdp,
            manufacturerId: selectedManufacturer.id,
            socketId: selectedSocket.id,
        }).then(() => {
            setName('')
            setReleaseYear('')
            setCore('')
            setClock('')
            setCache('')
            setThread('')
            setTdp(1)
            setProcess('')
            setRamMaxClock(1)
            setSelectedManufacturer({})
            setSelectedSocket({})
            onHide()
        })
    }

    return (<Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Додавання процесору</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Виробник</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedManufacturer.name || 'Оберіть виробника'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.manufacturers.map(manufacturer =>
                                <Dropdown.Item
                                    key={manufacturer.id}
                                    onClick={() => setSelectedManufacturer(manufacturer)}
                                >{manufacturer.name}</Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Назва</Form.Label>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={'Назва'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Сокет</Form.Label>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{selectedSocket.name || 'Оберіть сокет'}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {parts.sockets_.map(socket =>
                                <Dropdown.Item
                                    key={socket.id}
                                    onClick={() => setSelectedSocket(socket)}
                                >{socket.name}</Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Рік випуску</Form.Label>
                    <Form.Control
                        value={releaseYear}
                        onChange={e => setReleaseYear(e.target.value)}
                        placeholder={'Рік випуску (напр. 2018)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть ядер</Form.Label>
                    <Form.Control
                        value={core}
                        onChange={e => setCore(e.target.value)}
                        placeholder={'Кіл-сть ядер (напр. 8)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть потоків</Form.Label>
                    <Form.Control
                        value={thread}
                        onChange={e => setThread(e.target.value)}
                        placeholder={'Кіл-сть потоків (напр. 16)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Частота</Form.Label>
                    <Form.Control
                        value={clock}
                        onChange={e => setClock(e.target.value)}
                        placeholder={'Частота (напр. 2.8)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Величина тех. процесу</Form.Label>
                    <Form.Control
                        value={process}
                        onChange={e => setProcess(e.target.value)}
                        placeholder={'Величина тех. процесу (напр. 8)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть кешу 3 рівня</Form.Label>
                    <Form.Control
                        value={cache}
                        onChange={e => setCache(e.target.value)}
                        placeholder={'Кіл-сть кешу 3 рівня (напр. 32)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Максмально допустима частота RAM (напр. 3600)</Form.Label>
                    <Form.Control
                        type="number"
                        value={ramMaxClock}
                        onChange={e => setRamMaxClock(parseInt(e.target.value))}
                        placeholder={'Максмально допустима частота RAM (напр. 3600)'}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Кіл-сть тепла, що порібно розсіювати (напр. 75)</Form.Label>
                    <Form.Control
                        type="number"
                        value={tdp}
                        onChange={e => setTdp(parseInt(e.target.value))}
                        placeholder={'Кіл-сть тепла, що порібно розсіювати (напр. 75)'}
                    />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Відмінити
            </Button>
            <Button variant="primary" onClick={handleCreateCpu}>
                Зберегти
            </Button>
        </Modal.Footer>
    </Modal>);
};

export default CreateCpu;