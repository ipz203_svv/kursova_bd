import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createRamType} from "../../http/ramTypeAPI";

const CreateRamType = ({show, onHide}) => {
    const [value, setValue] = useState('');

    const handleCreateRamType = () => {
        createRamType({name: value}).then(() => {
                setValue('')
                onHide()
            }
        )
    }

    return (
        <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Додавання типу пам'яті</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={'Назва типу (напр. DDR1)'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Відмінити
                </Button>
                <Button variant="primary" onClick={handleCreateRamType}>
                    Зберегти
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateRamType;