import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createManufacturer} from "../../http/manufacturerAPI";

const CreateManufacturer = ({show, onHide}) => {
    const [value, setValue] = useState('');

    const handleCreateManufacturer = () => {
        createManufacturer({name: value}).then(() => {
                setValue('')
                onHide()
            }
        )
    }

    return (
        <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Додавання виробника</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={'Назва виробника'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Відмінити
                </Button>
                <Button variant="primary" onClick={handleCreateManufacturer}>
                    Зберегти
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateManufacturer;