import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createFanSize} from "../../http/fanSizeAPI";

const CreateFanSize = ({show, onHide}) => {
    const [value, setValue] = useState('');

    const handleCreateFanSize = () => {
        createFanSize({size: value}).then(() => {
                setValue('')
                onHide()
            }
        )
    }

    return (
        <Modal show={show} onHide={onHide} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Додавання розміру вентилятора</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={'Назва розміру (напр. 120mm)'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Відмінити
                </Button>
                <Button variant="primary" onClick={handleCreateFanSize}>
                    Зберегти
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateFanSize;