import {observer} from "mobx-react-lite";
import React, {useContext} from 'react';
import {Pagination} from "react-bootstrap";
import {Context} from "../../index";

const PaginatorPowerSupply = observer(() => {
    const {pagination} = useContext(Context)
    let items = [];
    const pagesCount = Math.ceil(pagination.powerSupplyTotalCount / pagination.powerSupplyLimit)
    for (let i = 0; i < pagesCount; i++) {
        items.push(i + 1);
    }

    return (<div>
        <Pagination>
            {items.map(i =>
                <Pagination.Item
                    key={i}
                    active={i === pagination.powerSupplyActivePage}
                    onClick={() => pagination.powerSupplyActivePage = i}>
                    {i}
                </Pagination.Item>
            )}
        </Pagination>
    </div>);
});

export default PaginatorPowerSupply;