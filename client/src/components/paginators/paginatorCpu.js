import {observer} from "mobx-react-lite";
import React, {useContext} from 'react';
import {Pagination} from "react-bootstrap";
import {Context} from "../../index";

const PaginatorCpu = observer(() => {
    const {pagination} = useContext(Context)
    let items = [];
    const pagesCount = Math.ceil(pagination.cpuTotalCount / pagination.cpuLimit)
    for (let i = 0; i < pagesCount; i++) {
        items.push(i + 1);
    }

    return (<div>
        <Pagination>
            {items.map(i =>
                <Pagination.Item
                    key={i}
                    active={i === pagination.cpuActivePage}
                    onClick={() => pagination.cpuActivePage = i}>
                    {i}
                </Pagination.Item>
            )}
        </Pagination>
    </div>);
});

export default PaginatorCpu;