import {observer} from "mobx-react-lite";
import React, {useContext} from 'react';
import {Pagination} from "react-bootstrap";
import {Context} from "../../index";

const PaginatorGpu = observer(() => {
    const {pagination} = useContext(Context)
    let items = [];
    const pagesCount = Math.ceil(pagination.gpuTotalCount / pagination.gpuLimit)
    for (let i = 0; i < pagesCount; i++) {
        items.push(i + 1);
    }

    return (<div>
        <Pagination>
            {items.map(i =>
                <Pagination.Item
                    key={i}
                    active={i === pagination.gpuActivePage}
                    onClick={() => pagination.gpuActivePage = i}>
                    {i}
                </Pagination.Item>
            )}
        </Pagination>
    </div>);
});

export default PaginatorGpu;