import React, {createContext} from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals';
import AssemblyStore from "./stores/assemblyStore";
import FilteringStore from "./stores/filteringStore";
import PaginationStore from "./stores/paginationStore";
import PartsStore from "./stores/partsStore";
import UserStore from "./stores/userStore";

export const Context = createContext(null)
const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<Context.Provider value={{
    parts: new PartsStore(),
    user: new UserStore(),
    assembly: new AssemblyStore(),
    pagination: new PaginationStore(),
    filtering: new FilteringStore()
}}>
    <App/>
</Context.Provider>);

reportWebVitals();
