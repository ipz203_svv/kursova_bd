export function logOut(user) {
    user.setUser({})
    user.setIsAuth(false)
    localStorage.removeItem('token')
}