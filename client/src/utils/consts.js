export const HOME_ROUTE = '/home'
export const ASSEMBLY_ROUTE = '/assembly'
export const ADMIN_ROUTE = '/admin'

export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const PROFILE_ROUTE = '/profile'

export const CPU_ROUTE = '/cpu'
export const GPU_ROUTE = '/gpu'
export const MOTHERBOARD_ROUTE = '/motherboard'
export const CPU_COOLER_ROUTE = '/cpu_cooler'
export const CASE_ROUTE = '/case'
export const CASE_FAN_ROUTE = '/case_fan'
export const RAM_ROUTE = '/ram'
export const POWER_SUPPLY_ROUTE = '/power_supply'
export const SSD_ROUTE = '/ssd'

export const GLOBAL_LIMIT = 5